/**
 * 
 */
package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxEvent;
import com.mxgraph.view.mxGraph;

import controller.Controller;
import model.Model;
import model.MyWeightedEdge;

/**
 * @author PaulArthur
 *
 */
public class MainView extends JFrame{
	
	/**
	 * serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Default size of the view
	 */
	public static final Dimension DEFAULT_SIZE = new Dimension(688, 480);
	/**
	 * Instance of the model
	 */
	private Model model;
	/**
	 * Icon of the view
	 */
	private ImageIcon icon;
	/**
	 * x coordinate where to add a new vertex
	 */
	private double x = 30;
	/**
	 * y coordinate where to add a new vertex
	 */
	private double y = 80;
	/**
	 * width of the new vertex
	 */
	private double width = 50;
	/**
	 * height of the new vertex
	 */
	private double height = 50;
    /**
     * A new vertex should be added to the graph
     */
    private static final String ADD_EDGE = "2";
    /**
     * A new vertex should be added to the graph
     */
    private static final String ADD_VERTEX = "1";
    /**
     * The graph shold be reset to his empty value
     */
    private static final String RESET_ALL = "0";
    /**
     * A vertex or Edge should be deleted from the graph
     */
    private static final String DELETE = "3";
	/**
	 * Light gray color
	 */
	private final Color lightGray = new Color(220,220,220);

	/**
	 * Title of text area
	 */
	private final String titleOfTextArea = "<html><h2 align = \"center\"><font face = \"Arial\" size = \"5\" color=\"blue\">The Chinese Postman Game</h2><hr><br><br><img src='" + getClass().getResource("Postboten.png").toString()+"' align = 'middle'/><font face = \"Arial\" size = \"5\"> Hey dear, <br>";
	private final String defaultText = titleOfTextArea+" <body>to get started you first of all need to create or load a graph with at least 2 vertices. To do so, you can either use the following buttons <b>\"Add Vertex\"</b>, <b>\"Add Edge\"</b> or you can choose the <b>\"Load Graph\"</b> option in the Menu <b>\"File\"</b> to select a graph. When you have created the graph then press the <b>\"Start\"</b> button below.<br>If you need some help you can check the tutorial or the lexicon in the <b>Help</> Menu.";
	private mxGraph graph = new mxGraph();
	private mxGraphComponent component;
	private JTextField textField;
	private JEditorPane textArea;
	private JScrollPane scrollPane;
	private JPanel graphPanel;
	private JPanel startPanel;
	private JPanel cPanel;
	private JPanel wPanel;
	private JButton buttonEdge;
	private JButton buttonDel;
	private JButton buttonAdd;
	private JButton buttonStart;
	private JButton buttonOk;
	private JButton buttonCancelLastMove;
	private JButton buttonReset;
	private JMenu menuFile, menuLayouts, help;
    private JMenuItem newItem, loadItem, saveItem, saveAsItem, exitItem,hierarchical, cycle, fastOrganic, organic, parallelEdge, lexicon, tutorial, about;
    private JMenuBar mb=new JMenuBar();  
    
	/**
	 * Creates anew object of the View class
	 */
	public MainView(Model model) {
		
		super("Chinese Postman Game");
		this.model = model;
		
		initGUI();
		this.model.addObserver(new ModelObserver());
		
	}

	/**
	 * Initialize the GUI
	 */
	private void initGUI() {
		
		//initialize
		try {
			icon = new ImageIcon(ImageIO.read(getClass().getResource("Postboten.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		setIconImage(icon.getImage());
		graphPanel = new JPanel();
		graphPanel.setBorder(new EmptyBorder(10, 0, 10, 0));
		startPanel = new JPanel();
		startPanel.setBorder(new EmptyBorder(10, 0, 10, 0));
		cPanel = new JPanel();
		cPanel.setLayout(new BoxLayout(cPanel, BoxLayout.Y_AXIS));
		cPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		wPanel = new JPanel();
		wPanel.setLayout(new BoxLayout(wPanel, BoxLayout.Y_AXIS));
		wPanel.setBorder(new EmptyBorder(10, 0, 10, 10));
		
		setSize(1050,620);
		setLocationRelativeTo(null);
		component = new mxGraphComponent(graph);
		component.setPreferredSize(new Dimension(688, 540));
		component.getViewport().setBackground(Color.white);
		component.setConnectable(false);
		setLayout(new BorderLayout());
		
		add(cPanel,BorderLayout.CENTER);
		add(wPanel,BorderLayout.EAST);
		setJMenuBar(mb);
		graph.setEdgeLabelsMovable(false);
		graph.setDisconnectOnMove(false);
		graph.setAllowDanglingEdges(false);
		
		//menu bar items
		menuFile = new JMenu("File");    
	    newItem = new JMenuItem("New Graph");  
	    loadItem = new JMenuItem("Load Graph...");  
	    saveItem = new JMenuItem("Save");  
	    saveAsItem = new JMenuItem("Save as...");  
	    exitItem = new JMenuItem("Exit");  
	    menuFile.add(newItem); menuFile.add(loadItem); menuFile.add(saveItem); menuFile.add(saveAsItem); menuFile.add(exitItem); 
	    mb.add(menuFile);  
	    
	    //menu bar Layouts
	    menuLayouts = new JMenu("Graph's Layouts");
	    hierarchical = new JMenuItem("Hierarchic");  
	    cycle = new JMenuItem("Cycle");  
	    fastOrganic = new JMenuItem("FastOrganic");  
	    organic = new JMenuItem("Organic");  
	    parallelEdge = new JMenuItem("ParallelEdge");  
	    menuLayouts.add(hierarchical); menuLayouts.add(cycle); menuLayouts.add(fastOrganic); menuLayouts.add(organic); menuLayouts.add(parallelEdge);
	    mb.add(menuLayouts);
	    menuLayouts.setEnabled(false);
	    
	    //menu bar help
	    help = new JMenu("Help");
	    lexicon = new JMenuItem("Lexicon");
	    tutorial = new JMenuItem("Tutorial");
	    about = new JMenuItem("About");
	    help.add(tutorial);
	    help.add(lexicon);
	    help.add(about);
	    mb.add(help);

	    //Text area and Scrollpane
  		textArea = new JEditorPane();
  		textArea.setEditable(false);
  		textArea.setBorder(new EmptyBorder(5, 5, 150, 5));
  		textArea.setContentType("text/html");
  		textArea.setText(defaultText);
  		scrollPane = new JScrollPane(textArea);
  		scrollPane.setPreferredSize(new Dimension(315, 494));
  		
  		//Graph Panel
  		buttonAdd = new JButton("Add Vertex");
  		graphPanel.add(buttonAdd);

  		buttonEdge = new JButton("Add Edge");
  		graphPanel.add(buttonEdge);

  		buttonDel = new JButton("Delete");
  		graphPanel.add(buttonDel);

  		textField = new JTextField("write name of vertex");
  		textField.setPreferredSize(new Dimension(410,21));
  		graphPanel.add(textField);
  		buttonReset = new JButton("Reset");
  		buttonCancelLastMove = new JButton("Cancel Last Move");
  		
  		//cPanel
  		cPanel.add(component);
  		cPanel.add(graphPanel);
  		
  		//startPanel
  		buttonStart = new JButton("Start");
  		buttonOk = new JButton("OK");
  		buttonStart.setEnabled(false);
  		buttonOk.setEnabled(false);
  		startPanel.add(buttonStart);
  		startPanel.add(buttonOk);
  		
  		
  		//wPanel
  		wPanel.add(scrollPane);
  		wPanel.add(startPanel);

  		setViewFont(new Font("Arial", Font.BOLD, 12));
    	setResizable(false);
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  		setPaneColor(lightGray);

	}

	/**
	 * Set the font of everything in the app
	 * However the font size of textfield and textarea are a little bit bigger
	 * @param font the font to set
	 */
	private void setViewFont(Font font) {
		menuFile.setFont(font);
		newItem.setFont(font);
		loadItem.setFont(font); 
		saveItem.setFont(font);
		saveAsItem.setFont(font);
		exitItem.setFont(font);
		lexicon.setFont(font);
		about.setFont(font);
		tutorial.setFont(font);
		help.setFont(font);
		menuLayouts.setFont(font);
		hierarchical.setFont(font);
		cycle.setFont(font);
		fastOrganic.setFont(font);
		organic.setFont(font);
		textArea.setFont(new Font("Arial", Font.BOLD, 15));
		parallelEdge.setFont(font);
		buttonAdd.setFont(font);
		buttonEdge.setFont(font);
		buttonDel.setFont(font);
		buttonOk.setFont(font);
		buttonStart.setFont(font);
		buttonReset.setFont(font);
		buttonCancelLastMove.setFont(font);
		textField.setFont(new Font("Arial", Font.ITALIC, 15));
	}
	
	/**
	 * Add buttons to help bz the algorithm.
	 */
	public void addAlgoButtons() {
		
		graphPanel.removeAll();
		graphPanel.add(buttonCancelLastMove);
		graphPanel.add(buttonReset);
		
		graphPanel.revalidate();
		graphPanel.repaint();
		pack();
	}
	
	/**
	 * Add buttons to create a graph.
	 */
	public void addGraphButtons() {
		
		graphPanel.removeAll();
		graphPanel.add(buttonAdd);
		graphPanel.add(buttonEdge);
		graphPanel.add(buttonDel);
		graphPanel.add(textField);
		graphPanel.revalidate();
		graphPanel.repaint();
		pack();
	}
	/**
	 * Set the color of the panes
	 * @param color
	 */
	private void setPaneColor(Color color) {
		cPanel.setBackground(color);
		wPanel.setBackground(color);
		startPanel.setBackground(color);
		graphPanel.setBackground(color);
	}
	
	/**
	 * Add a focus listener for the textfield
	 * @param c
	 */
	public void addFocusListener(Controller c) {
		textField.addFocusListener(c);
	}
	
	/**
	 * Add a selection listener for the graph
	 * @param c
	 */
	public void addCellSelectionListener(Controller c) {
		graph.getSelectionModel().addListener(mxEvent.CHANGE, c);
	}
	
	/**
	 * Add an action listener for the delete button
	 * @param listenForDeleteButton
	 */
	public void addDeleteListener(Controller listenForDeleteButton) {
		buttonDel.addActionListener(listenForDeleteButton);
	}
	
	/**
	 * Add an action listener for the reset button
	 * @param listenForResetButton
	 */
	public void addResetListener(Controller listenForResetButton) {
		buttonReset.addActionListener(listenForResetButton);
	}
	
	/**
	 * Add an action listener for the reset last move button
	 * @param listenForCancelLastMoveButton
	 */
	public void addCancelLastMoveListener(Controller listenForCancelLastMoveButton) {
		buttonCancelLastMove.addActionListener(listenForCancelLastMoveButton);
	}
	
	/**
	 * Add an action listener for the Ok button
	 * @param listenForOkButton
	 */
	public void addOkListener(Controller listenForOkButton) {
		buttonOk.addActionListener(listenForOkButton);
	}
	
	/**
	 * Add an action listener for the Start button
	 * @param listenForStartButton
	 */
	public void addStartListener(Controller listenForStartButton) {
		buttonStart.addActionListener(listenForStartButton);
	}
	
	/**
	 * Add an action listener for the hierarchical Layout Item
	 * @param listenForHierarchicalItem
	 */
	public void addhierListener(Controller listenForHierarchicalItem) {
		hierarchical.addActionListener(listenForHierarchicalItem);
	}
	
	/**
	 * Add an action listener for the lexicon Item
	 * @param listenForHelpItem
	 */
	public void addHelpItemListener(Controller listenForHelpItem) {
		lexicon.addActionListener(listenForHelpItem);
	}
	
	/**
	 * Add an action listener for the About Item
	 * @param listenForAboutItem
	 */
	public void addAboutItemListener(Controller listenForAboutItem) {
		about.addActionListener(listenForAboutItem);
	}
	
	/**
	 * Add an action listener for the tutorial Item
	 * @param listenForTutorialItem
	 */
	public void addTutorialItemListener(Controller listenForTutorialItem) {
		tutorial.addActionListener(listenForTutorialItem);
	}
	
	/**
	 * Add an action listener for the cycle Layout Item
	 * @param listenForCicleItem
	 */
	public void addCycleListener(Controller listenForCicleItem) {
		cycle.addActionListener(listenForCicleItem);
	}
	
	/**
	 * Add an action listener for the fast organic Layout Item
	 * @param listenForFastOrganicItem
	 */
	public void addFastOListener(Controller listenForFastOrganicItem) {
		fastOrganic.addActionListener(listenForFastOrganicItem);
	}
	
	/**
	 * Add an action listener for the organic Layout Item
	 * @param listenForOrganicItem
	 */
	public void addOrganicListener(Controller listenForOrganicItem) {
		organic.addActionListener(listenForOrganicItem);
	}
	
	/**
	 * Add an action listener for the parallel edge Layout Item
	 * @param listenForParallelEdgeItem
	 */
	public void addParEdgeListener(Controller listenForParallelEdgeItem) {
		parallelEdge.addActionListener(listenForParallelEdgeItem);
	}
	
	/**
	 * Add an action listener for the New Item
	 * @param listenForNewItem
	 */
	public void addNewItemListener(Controller listenForNewItem) {
		newItem.addActionListener(listenForNewItem);
	}
	
	/**
	 * Add an action listener for the Load Item
	 * @param listenForLoadItem
	 */
	public void addLoadItemListener(Controller listenForLoadItem) {
		loadItem.addActionListener(listenForLoadItem);
	}
	
	/**
	 * Add an action listener for the Save Item
	 * @param listenForSaveItem
	 */
	public void addSaveItemListener(Controller listenForSaveItem) {
		saveItem.addActionListener(listenForSaveItem);
	}
	
	/**
	 * Add an action listener for the Save as Item
	 * @param listenForSaveAsItem
	 */
	public void addSaveAsItemListener(Controller listenForSaveAsItem) {
		saveAsItem.addActionListener(listenForSaveAsItem);
	}
	
	/**
	 * Add an action listener for the Exit Item
	 * @param listenForExitItem
	 */
	public void addExitItemListener(Controller listenForExitItem) {
		exitItem.addActionListener(listenForExitItem);
	}
	
	/**
	 * Add an action listener for the Add vertex button
	 * @param listenForDeleteButton
	 */
	public void addVertexListener(Controller listenForDeleteButton) {
		buttonAdd.addActionListener(listenForDeleteButton);
	}
	
	/**
	 * Add an action listener for the Add edge button
	 * @param listenForDeleteButton
	 */
	public void addEdgeListener(Controller listenForDeleteButton) {
		buttonEdge.addActionListener(listenForDeleteButton);
	}

	/**
	 * @return the graph
	 */
	public mxGraph getGraph() {
		return graph;
	}

	/**
	 * @return the component
	 */
	public mxGraphComponent getComponent() {
		return component;
	}

	/**
	 * @return the textField
	 */
	public JTextField getTextField() {
		return textField;
	}

	/**
	 * @return the textPane
	 */
	public JEditorPane getTextPane() {
		return textArea;
	}

	/**
	 * @return the buttonEdge
	 */
	public JButton getButtonEdge() {
		return buttonEdge;
	}

	/**
	 * @return the buttonDel
	 */
	public JButton getButtonDel() {
		return buttonDel;
	}

	/**
	 * @return the buttonAdd
	 */
	public JButton getButtonAdd() {
		return buttonAdd;
	}

	/**
	 * @return the buttonStart
	 */
	public JButton getButtonStart() {
		return buttonStart;
	}

	/**
	 * @return the buttonOk
	 */
	public JButton getButtonOk() {
		return buttonOk;
	}

	/**
	 * @return the newItem
	 */
	public JMenuItem getNewItem() {
		return newItem;
	}

	/**
	 * @return the loadItem
	 */
	public JMenuItem getLoadItem() {
		return loadItem;
	}

	/**
	 * @return the saveItem
	 */
	public JMenuItem getSaveItem() {
		return saveItem;
	}

	/**
	 * @return the saveAsItem
	 */
	public JMenuItem getSaveAsItem() {
		return saveAsItem;
	}

	/**
	 * @return the exitItem
	 */
	public JMenuItem getExitItem() {
		return exitItem;
	}

	/**
	 * @return the hierarchical
	 */
	public JMenuItem getHierarchical() {
		return hierarchical;
	}

	/**
	 * @return the cicle
	 */
	public JMenuItem getCicle() {
		return cycle;
	}

	/**
	 * @return the fastOrganic
	 */
	public JMenuItem getFastOrganic() {
		return fastOrganic;
	}

	/**
	 * @return the organic
	 */
	public JMenuItem getOrganic() {
		return organic;
	}

	/**
	 * @return the parallelEdge
	 */
	public JMenuItem getParallelEdge() {
		return parallelEdge;
	}
	
	/**
	 * @return the menuLayouts
	 */
	public JMenu getMenuLayouts() {
		return menuLayouts;
	}

	/**
	 * @return the titleOfTextArea
	 */
	public String getTitleOfTextArea() {
		return titleOfTextArea;
	}

	/**
	 * @return the defaultText
	 */
	public String getDefaultText() {
		return defaultText;
	}

	/**
	 * @return the helpItem
	 */
	public JMenuItem getHelpItem() {
		return lexicon;
	}
	
	/**
	 * @return the aboutItem
	 */
	public JMenuItem getAboutItem() {
		return about;
	}
	
	/**
	 * @return the tutorial item
	 */
	public JMenuItem getTutorialItem() {
		return tutorial;
	}

	/**
	 * @return the graphPanel
	 */
	public JPanel getGraphPanel() {
		return graphPanel;
	}

	/**
	 * @return the buttonBackOffLastMove
	 */
	public JButton getButtonBackOffLastMove() {
		return buttonCancelLastMove;
	}

	/**
	 * @return the buttonReset
	 */
	public JButton getButtonReset() {
		return buttonReset;
	}
	
	/**ModelObserver is the class observing any changes in the model and modifying the view accordingly
	 * 
	 * @author PaulArthur
	 */
	private class ModelObserver implements Observer {

        @Override
        public void update(Observable o, Object arg) {
            if (arg != null) {
            	ArrayList<Object> a = (ArrayList<Object>) arg;
            	
            	if(String.valueOf(a.get(0)).equals(ADD_VERTEX)) {
	            	x=x+30;
	        		graph.getModel().beginUpdate();
	        		
	        		Object v1 = graph.insertVertex(graph.getDefaultParent(), null, String.valueOf(a.get(1)), x, y, width, height,"shape=ellipse;perimeter=ellipsePerimeter;whiteSpace=wrap;editable=0;");//vertex.toString() = name+label
	        	
	        		graph.getModel().endUpdate();
	        		
        		}else if(String.valueOf(a.get(0)).equals(ADD_EDGE)) {
        			
        			Object[] cells = graph.getChildCells(graph.getDefaultParent());
	            	MyWeightedEdge edge = (MyWeightedEdge) (a.get(1));
					Object v1 = null;
					Object v2 = null;
					boolean sourceFound = false;
					boolean targetFound = false;
					boolean normalEdge = true;
					
					if(a.size()>2) {
						normalEdge = false;
					}
					for(int k = 0; k<cells.length; k++) {
						mxCell cell = (mxCell)cells[k];
						if(cell.isVertex()&& cell.getValue().equals(edge.getSource().getName())) {
							v1 = cells[k];
							sourceFound = true;
							if(targetFound) {
								break;
							}
						}
						if(cell.isVertex()&& cell.getValue().equals(edge.getTarget().getName())) {
							v2 = cells[k];
							targetFound = true;
							if(sourceFound) {
								break;
							}
						}
					}
					
	        		graph.getModel().beginUpdate();
	        		
	        		if(normalEdge) {
		        		if(edge.getDirected()) {
		        			Object edgeCell = graph.insertEdge(graph.getDefaultParent(), edge.getID(), edge.getWeight(), v1, v2,"editable=0;");
		        		}else {
		        			Object edgeCell = graph.insertEdge(graph.getDefaultParent(), edge.getID(), edge.getWeight(), v1, v2,"endArrow=none;editable=0;");
		        		}
	        		}else {
	        			if(edge.getDirected()) {
		        			Object edgeCell = graph.insertEdge(graph.getDefaultParent(), edge.getID(), edge.getWeight(), v1, v2, String.valueOf(a.get(2))+"editable=0;");
		        			graph.getModel().setVisible(edgeCell, (boolean) a.get(3));
		        		}else {
		        			Object edgeCell = graph.insertEdge(graph.getDefaultParent(), edge.getID(), edge.getWeight(), v1, v2, String.valueOf(a.get(2))+"endArrow=none;editable=0;");
		        			graph.getModel().setVisible(edgeCell, (boolean) a.get(3));
		        		}
	        		}
	        		
	        		graph.getModel().endUpdate();
	        		
	            } else if(String.valueOf(a.get(0)).equals(DELETE)) {
	            	Object[] cells = graph.getChildCells(graph.getDefaultParent());
	            	
					for(int k = 0; k<cells.length; k++) {
						mxCell cell = (mxCell)cells[k];
						if(cell.isVertex()&& cell.getValue().equals(String.valueOf(a.get(1)))) {
							
							graph.getModel().remove(cell);
							break;
						}else if(cell.isEdge()&& cell.getId().equals(String.valueOf(a.get(1)))) {
							
							graph.getModel().remove(cell);
							break;
						}
					}
				}else if(String.valueOf(a.get(0)).equals(RESET_ALL)) {
		        	 Object[] cells = graph.getChildCells(graph.getDefaultParent());
			    	 graph.removeCells(cells, true);
		        }
	        }else {
					try {
						throw new UnknownProtokoll();
					} catch (UnknownProtokoll e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	        }
    	}
    }
	
}