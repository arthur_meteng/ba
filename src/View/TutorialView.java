/**
 * 
 */
package View;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;


/**
 * @author PaulArthur
 *
 */
public class TutorialView  extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JEditorPane textArea;
	private JScrollPane scr;
	private ImageIcon icon;
	private final String titleOfTextArea = "<html><h1 align = \"center\">Tutorial for the game</h1>"
			+ "<hr><br><br>"
			+ "<img src='" + getClass().getResource("Postboten.png").toString()+"' align = 'middle'/>"
			+ "<font face = \"Arial\" size = \"4\"> Hey dear, <br>";
	
	private final static Color lightGray = new Color(220,220,220);
	
	/**
	 * 
	 */
	public TutorialView() {
		super("Tutorial");
		setLayout(new BorderLayout());
		try {
			icon = new ImageIcon(ImageIO.read(getClass().getResource("Postboten.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		setIconImage(icon.getImage());  
		textArea = new JEditorPane();
		textArea.setEditable(false);
		scr = new JScrollPane(textArea);
		scr.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		HTMLEditorKit kit = new HTMLEditorKit();
		textArea.setEditorKit(kit);
		
		StyleSheet styleSheet = kit.getStyleSheet();
		styleSheet.addRule("body {color:#000; font-family:times; margin: 4px; }");
        styleSheet.addRule("h1 {color: blue;}");
        styleSheet.addRule("h2 {color: #ff0000;}");
        styleSheet.addRule("pre {font : 10px monaco; color : black; background-color : #fafafa; }");

        String text = titleOfTextArea + "<body>"
                + "<p>Welcome to this tutorial! <br>To play the game you will first of all need to choose the kind of graph you want to play with.</p>"
                + "<br>"
    			+ "<center><img src='" + getClass().getResource("choice.png").toString()+"' align = 'middle'/></center>"
    			+ "<br><br>"
    			+ "<p>As soon as you choosed the type of graph, you will have the display shown with his different parts as describe in the image below.</p>"
    			+ "<br>"
    			+ "<center><img src='" + getClass().getResource("display.png").toString()+"' align = 'middle'/></center>"
    			+ "<br><br>"
    			+ "<p>You can always change the type of graph by selecting the new Graph menu option as shown below.</p>"
    			+ "<br>"
    			+ "<center><img src='" + getClass().getResource("new.png").toString()+"' align = 'middle'/></center>"
    			+ "<br><br>"
    			+ "<p>You can load or save a graph by clicking on the \"Load Graph...\" menu item for loading and on the \"Save\" or \"Save as...\" menu items for saving a graph.</p>"
    			+ "<br>"
    			+ "<center><img src='" + getClass().getResource("laden.png").toString()+"' align = 'middle'/></center>"
    			+ "<br><br>"
    			+ "<p>There is also, in addition to this tutorial, a lexicon in the help menu, where you can check the definition of some words used during the game. There is also some informations about the game in the \"About\" menu item.</p>"
    			+ "<br>"
    			+ "<center><img src='" + getClass().getResource("help.png").toString()+"' align = 'middle'/></center>"
    			+ "<br><br>"
    			+ "<p>To create a graph, you can use the button shown below that are provided by the graphical interface. \"Add Vertex\" is used to add a vertex to your graph, but you need to enter the name of the vertex first. \"Add Edge\" is used to add some edge in your graph, you need to create at least 2 vertices before you can create an edge between them. The \"Delete\" button will remove the selected vertices or edges from your graph.</p>"
    			+ "<br>"
    			+ "<center><img src='" + getClass().getResource("createGraph.png").toString()+"' align = 'middle'/></center>"
    			+ "<br><br>"
    			+ "<p>If there is a negative cycle in your graph as it is in the picture below, you will have to change the weights of some edges to remove the negative cycle.</p>"
    			+ "<br>"
    			+ "<center><img src='" + getClass().getResource("negCycle.png").toString()+"' align = 'middle'/></center>"
    			+ "<br><br>"
    			+ "<p>If some vertices are not connected as it is in the picture below, you will need to add an edge to connecte them.</p>"
    			+ "<br>"
    			+ "<center><img src='" + getClass().getResource("notConnected.png").toString()+"' align = 'middle'/></center>"
    			+ "<br><br>"
    			+ "<p>If your graph is strongly connected without any negative cycles as it is below, you may continue with the game.</p>"
    			+ "<br>"
    			+ "<center><img src='" + getClass().getResource("beispiel.png").toString()+"' align = 'middle'/></center>"
    			+ "<br><br>"
    			+ "<p>You will then the have to answer if your graph is eulerian or not.</p>"
    			+ "<br>"
    			+ "<center><img src='" + getClass().getResource("eulerian.png").toString()+"' align = 'middle'/></center>"
    			+ "<br><br>"
    			+ "<p>If not you can let the programm add the missing paths to the actual graph as shown below, or you can choose to try building the eulerian cycle an add those edges (or paths) yourself.</p>"
    			+ "<br>"
    			+ "<center><img src='" + getClass().getResource("hinzufuegen.png").toString()+"' align = 'middle'/></center>"
    			+ "<br><br>"
    			+ "<p>In both cases you can now try to build the eulerian cycle by clicking on the different edges, knowing that the green vertex is your start point and the vertex with only the red borders is the one from where you can continue building the cycle.</p>"
    			+ "<br>"
    			+ "<center><img src='" + getClass().getResource("eulertour1.png").toString()+"' align = 'middle'/></center>"
    			+ "<br><br>"
    			+ "<p>You can fail if there are still unvisited edges that are no longer reachable from the start point.</p>"
    			+ "<br>"
    			+ "<center><img src='" + getClass().getResource("fehl.png").toString()+"' align = 'middle'/></center>"
    			+ "<br><br>"
    			+ "<p>You can also fail if the actual cost get bigger than the required one</p>\n"
    			+ "<br>"
    			+ "<center><img src='" + getClass().getResource("fehl2.png").toString()+"' align = 'middle'/></center>"
    			+ "<br><br>"
    			+ "<p>You will then need to cancel some moves or start from the beginning using the buttons shown below.</p>"
    			+ "<br>"
    			+ "<center><img src='" + getClass().getResource("cancel.png").toString()+"' align = 'middle'/></center>"
    			+ "<br><br>"
    			+ "<p>You win if the actual and required cost get equal and if there is no unvisited edges left.</p>"
    			+ "<br>"
    			+ "<center><img src='" + getClass().getResource("win.png").toString()+"' align = 'middle'/></center>"
    			+ "<br><br>"
                + "</body>";
        Document doc = kit.createDefaultDocument();
        textArea.setDocument(doc);
        textArea.setText(text);
        
        Container container = getContentPane();
        container.add(scr, BorderLayout.CENTER);
             
    	//Set the size of the JFrame window object
    	pack();
    	setSize(1170,620);
    	//Set the background color of the JFrame window object
    	container.setBackground(lightGray);
    	//Don't allow the user to resize the JFrame window object
    	setResizable(false);
    	//Make use the user can see the JFrame window object
    	setVisible(true);
    	

	}

}
