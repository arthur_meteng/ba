/**
 * 
 */
package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

/**
 * @author PaulArthur
 *
 */
public class AboutView extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JEditorPane textArea;
	private JScrollPane scr;
	private ImageIcon icon;
	private final String titleOfTextArea = "<html><h1 align = \"center\">About the Game</h1><hr><br><br><img src='" + getClass().getResource("Postboten.png").toString()+"' align = 'middle'/><font face = \"Arial\" size = \"4\"> Hey dear, <br>";
	
	private final static Color lightGray = new Color(220,220,220);
	
	
	public AboutView () {
		super("About");
		setLayout(new BorderLayout());
		try {
			icon = new ImageIcon(ImageIO.read(getClass().getResource("Postboten.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		setIconImage(icon.getImage());  
		textArea = new JEditorPane();
		textArea.setEditable(false);
		scr = new JScrollPane(textArea);
		scr.setBorder(new EmptyBorder(10, 10, 10, 10));
		HTMLEditorKit kit = new HTMLEditorKit();
		textArea.setEditorKit(kit);
		
		StyleSheet styleSheet = kit.getStyleSheet();
		styleSheet.addRule("body {color:#000; font-family:times; margin: 4px; }");
        styleSheet.addRule("h1 {color: blue;}");
        styleSheet.addRule("h2 {color: #ff0000;}");
        styleSheet.addRule("pre {font : 10px monaco; color : black; background-color : #fafafa; }");

        String text = titleOfTextArea+ "<body>\n"
                + "<p><b>The Chinese postman game</b> is a game based on the <b><i>Chinese Postman Problem</i></b>. This problem was posed in 1962 by the chinese mathematician <b><i>Mei-Ko Kwan</i></b>. It is about the problem that a chinese postman faces: he wishes to travel along every road in a city in order to deliver letters, with the least possible distance."
                + " The problem is how to find a shortest closed walk of the graph in which each edge is traversed at least once, rather than exactly once. In this game the user is about to help a postman named Mr. Ho to compute this short distance along a given graph.</p>"
                + "<br>"
                + "<p>This game was implement by <b><i>Paul Arthur Meteng Kamdem</i></b> as part of his Bachelor Degree. The progress in the game was inspired by a web application on the same subject (The Chinese Postman Problem). You can go to this web application <a href=\"https://www-m9.ma.tum.de/graph-algorithms/directed-chinese-postman/index_en.html\">here</a>."
    			+ "<br><br>"
                + "<p>The libraries used to implement the game are the java integrated Libraries <b>Swing</b> and <b>AWT</b> for the graphical interface. There was also external libraries like <b>Jgrapht</b> and <b>mxGraph</b> used to save the graph and draw it.</p>"
                + "</body>";
        Document doc = kit.createDefaultDocument();
        textArea.setDocument(doc);
        textArea.setText(text);
        
        Container container = getContentPane();
        container.add(scr, BorderLayout.CENTER);
        
        //Set the size of the JFrame window object
    	pack();
    	setSize(700,620);
    	//Set the background color of the JFrame window object
    	container.setBackground(lightGray);
    	//Don't allow the user to resize the JFrame window object
    	setResizable(false);
    	//Make use the user can see the JFrame window object
    	setVisible(true);
	}
	

}
