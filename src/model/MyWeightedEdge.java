package model;

import org.jgrapht.graph.DefaultWeightedEdge;

public class MyWeightedEdge extends DefaultWeightedEdge {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MyVertex source;
    private MyVertex target;
    private double weight;
    private boolean directed;
    private String ID;
    
    public MyWeightedEdge() {
    	super();
    }
	public MyWeightedEdge(MyVertex source, MyVertex target, double weight, String ID, boolean directed) {
	   this.ID=ID;
	   this.source = source;
	   this.target = target;
	   this.weight = weight;
	   this.directed = directed;
	}
	@Override
	public String toString()
    {
        return super.toString()+": "+super.getWeight();
    }
	
	public double getWeight() {
		return super.getWeight();
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public void setID(String ID) {
		this.ID = ID;
	}

	public MyVertex getSource() {
		return (MyVertex) super.getSource();
	}

	public MyVertex getTarget() {
		return (MyVertex) super.getTarget();
	}

	public String getID() {
		return ID;
	}
	
	public boolean getDirected() {
		return directed;
	}
	
	public void setDirected(boolean directed) {
		this.directed = directed;
		
	}
	
}
