package model;

import java.io.FileWriter;
import java.io.IOException;

import org.jgrapht.ListenableGraph;


/**
 * Creates a csv file from a JGraphT graph.
 *
 */
public class GraphSaver{

	/**
	 * Delimiter used in CSV file
	 */
	 private static final String COMMA_DELIMITER = ",";
	 private static final String NEW_LINE_SEPARATOR = "\n";
	/**
	 * CSV file header
	 */
	 private static final String FILE_HEADER = "sourceVertex,targetVertex,weight";
    /**
     * model used to update the intern data structure of graph as soon as a new one is created
     */
     private Model model;
    /**
     * Initializes a new {@link GraphSaver}.
     *
     * @param model
     */
    public GraphSaver(Model model){

        this.model = model;
    }

    /**Save a Graph from the model to a specified file
     * @param fileName
     * @throws NoSuchMethodException
     */
    public void saveGraph(String fileName) throws NoSuchMethodException {
    	FileWriter fWriter = null;
    	try {
    		fWriter = new FileWriter(fileName);
    		
    		//Write the CSV header
    		fWriter.append(FILE_HEADER.toString());
    		//Add a new line separator after the header
    		fWriter.append(NEW_LINE_SEPARATOR);
			if (model.graphIsDirected()) {
				saveGraphsEdges(model.getDiGraph(),fWriter);
			}else saveGraphsEdges(model.getUnDiGraph(),fWriter);
    		
    	}catch(Exception e){
    		e.printStackTrace();
    	}finally {
    		try {
				fWriter.flush();
				fWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
    		
    	}
       
    }

    /**
     * Saves a weighted edge into the csv file.
     * @param listenableGraph 
     * @param fWriter 
     * @throws IOException 
     *
     */
    protected void saveGraphsEdges(ListenableGraph<MyVertex, MyWeightedEdge> listenableGraph, FileWriter fWriter) throws IOException{
    	for(MyWeightedEdge e : listenableGraph.edgeSet()) {
			fWriter.append(listenableGraph.getEdgeSource(e).toString());
			fWriter.append(COMMA_DELIMITER);
			fWriter.append(listenableGraph.getEdgeTarget(e).toString());
			fWriter.append(COMMA_DELIMITER);
			fWriter.append(String.valueOf(listenableGraph.getEdgeWeight(e)));
			fWriter.append(NEW_LINE_SEPARATOR);
    	}
    }
}

