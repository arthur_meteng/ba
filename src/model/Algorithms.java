package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.jgrapht.Graph;
import org.jgrapht.ListenableGraph;
import org.jgrapht.alg.cycle.HierholzerEulerianCycle;
import org.jgrapht.alg.matching.KuhnMunkresMinimalWeightBipartitePerfectMatching;
import org.jgrapht.alg.shortestpath.FloydWarshallShortestPaths;
import org.jgrapht.graph.DefaultDirectedWeightedGraph;
import org.jgrapht.graph.DefaultListenableGraph;
import org.jgrapht.graph.DefaultUndirectedWeightedGraph;

public class Algorithms {

	private Model model;
	private Set<MyVertex> neg, pos, oddVerteices; // unbalanced vertices
	private Set<MyWeightedEdge> matchedUndirectEdges = new HashSet<MyWeightedEdge>();
	private FloydWarshallShortestPaths<MyVertex,MyWeightedEdge> shortestPath;
	private ListenableGraph<MyVertex, MyWeightedEdge> graph;
	private KuhnMunkresMinimalWeightBipartitePerfectMatching<MyVertex, MyWeightedEdge> matching;
	private HierholzerEulerianCycle<MyVertex, MyWeightedEdge> cycle;
	private static ArrayList<MyVertex> list = new ArrayList<>();
	
	private static double min = Double.MAX_VALUE;
	private double matchedCost = 0;
	private final String redColor = "#f41a1a";
	private final String greenColor = "#6dc066";
	private final String purpleColor = "#800080";
	private final String blueColor = "#0000ff";
	private final String yellowColor = "#ffd700";
	private final String marronColor = "#900102";
	private final String wColor = "#006666";
	private final String darkredColor = "#8B0000";
	private final String lightRedColor = "#f41a1a";
	private String matchingSet = "";
	private int count = 0;
	private HashMap<Integer, String> colMap;
	
	public Algorithms(Model model, ListenableGraph<MyVertex, MyWeightedEdge> graph) {
		
		pos = new HashSet<>();
	    neg = new HashSet<>();
	    oddVerteices = new HashSet<>();
		this.graph=graph;
		this.model = model;
        shortestPath = new FloydWarshallShortestPaths<MyVertex, MyWeightedEdge>(graph);
        cycle = new HierholzerEulerianCycle<MyVertex, MyWeightedEdge>();
		colMap = new HashMap<Integer, String>();
		colMap.put(0, blueColor);
		colMap.put(1, yellowColor);
		colMap.put(2, purpleColor);
		colMap.put(3, greenColor);
		colMap.put(4, redColor);
		colMap.put(5, marronColor);
		colMap.put(6, lightRedColor);
		colMap.put(7, wColor);
		colMap.put(8, darkredColor);
        
	}
	
	/**
	 * @return A set of the Edges from the matching
	 */
	public String getMatchingEdgesForDiGraph() {
		matchingSet = "[";
		int count = 0;
		int size = matching.getMatching().getEdges().size();
		Graph<MyVertex, MyWeightedEdge> graph2 = matching.getMatching().getGraph();
		for(MyWeightedEdge edge : matching.getMatching().getEdges()) {
			if(graph2.getEdgeSource(edge).name.contains(":")) {
				/**
				 * Reset the name of additional vertices that have been added during the matching so that it matches the name of the initial vertex.
				 * The two last character that have been added will be removed.
				 * This works only if we assume that all vertices don't have an absolute value of delta bigger than 10
				 **/
				graph2.getEdgeSource(edge).setName(String.valueOf(graph2.getEdgeSource(edge).name.substring(0, graph2.getEdgeSource(edge).name.length()-3)));
				System.out.println(graph2.toString());	
			}
			if(graph2.getEdgeTarget(edge).name.contains("-")) {
				//reset the name of additional vertices that have been added during the matching so that it matches the name of the initial vertex
				graph2.getEdgeTarget(edge).setName(String.valueOf(graph2.getEdgeTarget(edge).name.substring(0, graph2.getEdgeTarget(edge).name.length()-3)));
				System.out.println(graph2.toString());
			}
			
			if ((graph2.getEdgeSource(edge).name.contains(":"))&&(graph2.getEdgeTarget(edge).name.contains("-"))){
				graph2.getEdgeSource(edge).setName(String.valueOf(graph2.getEdgeSource(edge).name.substring(0, graph2.getEdgeSource(edge).name.length()-3)));
				graph2.getEdgeTarget(edge).setName(String.valueOf(graph2.getEdgeTarget(edge).name.substring(0, graph2.getEdgeTarget(edge).name.length()-3)));
			}	
			
			if(count==size-1) {
				matchingSet = matchingSet.concat("("+graph2.getEdgeSource(edge).name+"->"+graph2.getEdgeTarget(edge).name+")");
			}else {
				matchingSet = matchingSet.concat("("+graph2.getEdgeSource(edge).name+"->"+graph2.getEdgeTarget(edge).name+"), ");
			}
			matching.getMatching().getEdges(); 
			++count;
		}
		matchingSet = matchingSet.concat("]");
        return  matchingSet;        
	}
	
	/**
	 * Initialize a Hungarian object in oder to process the matching algorithm
	 */
	public void setHungarian() {
		initPartitions(); 
        Graph<MyVertex, MyWeightedEdge> graph2 = createBipartiteGraph();
        setMatching(new KuhnMunkresMinimalWeightBipartitePerfectMatching<MyVertex, MyWeightedEdge>(graph2, neg, pos));         
	}
	
	/**
	 * @return The total cost of the matching
	 */
	public double getMatchingCost() {
		return matching.getMatching().getWeight();
	}
	
	/**
	 * @return A set of the Edges from the matching
	 */
	public Set<MyWeightedEdge> getMatchingEdgesForUnDiGraph() {
		int numOfVertex = initOddVertices();
		Set<MyWeightedEdge> set = new HashSet<>();
		Graph<MyVertex, MyWeightedEdge> completeGraph = createCompleteGraph();
		set = buildMatching(numOfVertex, completeGraph);
		setMatchedUndirectEdges(set);
		return set;
	}
	
	/**
	 * Return a list of disconnected vertices in the graph and null when all vertices are connected.
	 */
	public ArrayList<String> getDisconnectedVertices() {
		ArrayList<String> vertices = new ArrayList<String>();
		for( MyVertex vertex1 : graph.vertexSet() ) {
			for( MyVertex vertex2 : graph.vertexSet() ) {
				if( !vertex1.equals(vertex2)) {
					if(shortestPath.getPath(vertex1, vertex2) == null) {
						vertices.add(vertex1.name);vertices.add(vertex2.name);
						return vertices;
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * Return true if the graph is strongly connected.
	 */
	public boolean isStronglyConnected() {	
		return getDisconnectedVertices()==null;
	}
	
	/**
	 * Return true if the graph is eulerian.
	 */
	public boolean isEulerian() {	
		return cycle.isEulerian(graph);
	}
	
	/**
	 * return true if the graph has no negative cycle
	 */
	public boolean hasNegativeCycle() {	
		
		return getNegativeCostVertex()!= null;
	}
	
	/**
	 * Return vertex's name in the graph with negative path cost and null when all there is no such a vertex.
	 */
	public String getNegativeCostVertex() {
		
		for( MyVertex vertex1 : graph.vertexSet() ) {
			if( shortestPath.getPathWeight(vertex1, vertex1) < 0 ) { 
				return vertex1.name;
			}
			
		}

		return null;
	}
	
	
	/**
	 * Create a complete bipartite graph.
	 * @return the created bipartite graph
	 * @throws UnsupportedOperationException
	 */
	Graph<MyVertex, MyWeightedEdge> createBipartiteGraph() throws UnsupportedOperationException{
		Graph<MyVertex, MyWeightedEdge> graph = new DefaultListenableGraph<>(new DefaultDirectedWeightedGraph<>(MyWeightedEdge.class));
		for(MyVertex source: neg) {
			for(MyVertex target :pos) {
				graph.addVertex(source);
				graph.addVertex(target);
				graph.setEdgeWeight(graph.addEdge(source, target), shortestPath.getPathWeight(source, target));
				
			}
		}
		//add some new source vertices with an edgeWeight of 0 to make the graph complete bipartite
		int count=0;
		for(MyVertex source: neg) {
			count=count+1;
			if(Math.abs(source.getDelta())!=1) {
				for(int i = 2; i<Math.abs(source.getDelta())+1;i++) {
					graph.addVertex(new MyVertex(source.name+":"+count+""+i));
					for(MyVertex v : graph.vertexSet()) {
						if(v.name.equals(source.name+":"+count+""+i)) {
							for(MyVertex target :pos) {
								graph.setEdgeWeight(graph.addEdge(v, target), 0);
								
							}
						}
					}
				}
				
			}
			count=0;
		}
		for(MyVertex v : graph.vertexSet()) {
			if(v.name.contains(":")) {
				neg.add(v);
			}
		}
	
		//add some new target vertices with an edgeWeight of 0 to make the graph complete bipartite
		
		for(MyVertex target: pos) {
			count=count+1;
			if(target.getDelta()!=1) {	
				for(int i = 2; i<target.getDelta()+1;i++) {
					graph.addVertex(new MyVertex(target.name+"-"+count+""+i));
					for(MyVertex v : graph.vertexSet()) {
						if(v.name.equals(target.name+"-"+count+""+i)) {
							for(MyVertex source :neg) {
								graph.setEdgeWeight(graph.addEdge(source, v), 0);
								
							}
						}
					}
				}
				
			}
		}
		for(MyVertex v : graph.vertexSet()) {
			if(v.name.contains("-")) {
				pos.add(v);
			}
		}
		System.out.println(graph.toString());
		return graph;
	}
	
	/**
	 * Build a matching from the given graph
	 * @param numberOfVertices
	 * @param graph
	 * @return the set of matched edges
	 */
	public Set<MyWeightedEdge> buildMatching(int numberOfVertices, Graph<MyVertex, MyWeightedEdge> graph){
		Set<MyWeightedEdge> edges = checkCombinations(graph.edgeSet().toArray(new MyWeightedEdge[graph.edgeSet().size()]), numberOfVertices/2, 0, new MyWeightedEdge[numberOfVertices/2], new HashSet<MyWeightedEdge>());
		min = Double.MAX_VALUE;
		return edges;
	
	}

	/**
	 * Compute and return the combination of edges with the minimal total weight.
	 * @param arr Array of all edges
	 * @param numberOfEdges Number of edges within a combination
	 * @param startPosition The start position in the array of all edges from which we want to build the combinations
	 * @param result The resulting combination
	 * @param edges the result will be copied here
	 * @return The set of edges with the minimal total weight
	 */
	private static Set<MyWeightedEdge> checkCombinations(MyWeightedEdge[] arr, int numberOfEdges, int startPosition, MyWeightedEdge[] result, Set<MyWeightedEdge> edges){
		boolean overlap = false;
		double totalWeight = 0;
        if (numberOfEdges == 0){
        	for(int i = 0; i<result.length; i++) {
        		if(!list.contains(result[i].getSource())) {
        			list.add(result[i].getSource());
        			if(!list.contains(result[i].getTarget())) {
        				list.add(result[i].getTarget());
        				totalWeight += result[i].getWeight();
        			}else {
        				list.clear();
        				totalWeight = 0;
            			overlap=true;
            			break;
            		}
        		}else {
        			list.clear();
        			totalWeight = 0;
        			overlap=true;
        			break;
        		}
        	}
        	if(!overlap) {
        		if(totalWeight<min) {
        			edges.clear();
        			for(int i = 0; i<result.length; i++) {
        				edges.add(result[i]);
        			}
        			min = totalWeight;
        		}
        		
        	}
        	
            return edges;
        }       
        for (int i = startPosition; i <= arr.length-numberOfEdges; i++){
            result[result.length - numberOfEdges] = arr[i];
            checkCombinations(arr, numberOfEdges-1, i+1, result, edges);
        }
		return edges;
    }       
		
	/**
	 * Creates a complete graph from the vertex list of vertices with an odd number of edge
	 * @return the complete Graph
	 */
	public Graph<MyVertex, MyWeightedEdge> createCompleteGraph(){
		//add vertices with odd number of edges to the oddVertives List
		initOddVertices();
		
		Graph<MyVertex, MyWeightedEdge> graph = new DefaultListenableGraph<>(new DefaultUndirectedWeightedGraph<MyVertex, MyWeightedEdge>(MyWeightedEdge.class));
		for(MyVertex source: oddVerteices) {
			for(MyVertex target :oddVerteices) {
				String sName = source.name;
				String tName = target.name;
				if(!sName.equals(tName)) {
					graph.addVertex(source);
					graph.addVertex(target);
					MyWeightedEdge edge = graph.addEdge(source, target);
					double weight = shortestPath.getPathWeight(source, target);
					if(edge!=null) {
					graph.setEdgeWeight(edge,weight);
					}
				}
			}
		}
		
		return graph;
		
	}
	
	
	/**
	 * initialize the partition sets of unbalanced vertices
	 */
	public void initPartitions() {	
		for( MyVertex vertex : graph.vertexSet()) {
			vertex.setDelta(graph.outDegreeOf(vertex)-graph.inDegreeOf(vertex));
			if( (vertex.getDelta() < 0 )) neg.add(vertex);
			else if( (vertex.getDelta() > 0 )) pos.add(vertex);
		}
	}
	
	/**
	 * initialize the set of vertices with an odd number of edges
	 * @return the size of the set
	 */
	 int initOddVertices() {	
		int count = 0;
		for( MyVertex vertex : graph.vertexSet()) {
			graph.edgesOf(vertex);
			if( (graph.edgesOf(vertex).size()%2 != 0 )) {
				oddVerteices.add(vertex);
				++count;
			}
		}
		return count;
	}
	 
		/**
		 * Add the matched edges to the directed graph
		 * If the parameter visible is true, so will be the edges otherwise the edges won't be visible
		 * @param visible
		 */
		public void addMatchedEdgeForDirectedGraph(boolean visible) {
			Graph<MyVertex, MyWeightedEdge> graph2 = matching.getMatching().getGraph();
			count = 0;
			int size = matching.getMatching().getEdges().size();
			for(MyWeightedEdge edge : matching.getMatching().getEdges()) {
				if(count<size) {
					if(graph2.getEdgeSource(edge).name.contains(":")) {
						/**
						 * Reset the name of additional vertices that have been added during the matching so that it matches the name of the initial vertex.
						 * The two last character that have been added will be removed.
						 * This works only if we assume that all vertices don't have an absolute value of delta bigger than 10
						 **/
						graph2.getEdgeSource(edge).setName(String.valueOf(graph2.getEdgeSource(edge).name.substring(0, graph2.getEdgeSource(edge).name.length()-3)));
						System.out.println(graph2.toString());	
					}
					if(graph2.getEdgeTarget(edge).name.contains("-")) {
						//reset the name of additional vertices that have been added during the matching so that it matches the name of the initial vertex
						graph2.getEdgeTarget(edge).setName(String.valueOf(graph2.getEdgeTarget(edge).name.substring(0, graph2.getEdgeTarget(edge).name.length()-3)));
						System.out.println(graph2.toString());
					}
					
					if ((graph2.getEdgeSource(edge).name.contains(":"))&&(graph2.getEdgeTarget(edge).name.contains("-"))){
						graph2.getEdgeSource(edge).setName(String.valueOf(graph2.getEdgeSource(edge).name.substring(0, graph2.getEdgeSource(edge).name.length()-3)));
						graph2.getEdgeTarget(edge).setName(String.valueOf(graph2.getEdgeTarget(edge).name.substring(0, graph2.getEdgeTarget(edge).name.length()-3)));
					}	
					
					count = addEdge(count, graph2, edge, visible);
				}
			}
			
		}
		
		/**
		 * Add the matched edges to the undirected graph.
		 * If the parameter visible is true, so will be the edges otherwise the edges won't be visible
		 * @param visible
		 */
		public void addMatchedEdgeForUnDirectedGraph(boolean visible) {
			count = 0;
			Graph<MyVertex, MyWeightedEdge> graph = createCompleteGraph();
			Set<MyWeightedEdge> edges = getMatchedUndirectEdges();
			for(MyWeightedEdge edge :edges) {
				if(count<edges.size()) {
					count = addEdge(count, graph, edge, visible);
				}		
			}
		}
		
		/**
		 * Add all edges along the path from source to target with a specific color 
		 * @param count
		 * @param graph2
		 * @param edge
		 * @param setVisible 
		 * @return count the incremented count
		 */
		private int addEdge(int count, Graph<MyVertex, MyWeightedEdge> graph2, MyWeightedEdge edge, boolean setVisible) {
				
				MyVertex s = model.getVertexByName(graph2.getEdgeSource(edge).name);
				MyVertex t = model.getVertexByName(graph2.getEdgeTarget(edge).name);
				if(count<colMap.size()) {
					for(MyWeightedEdge e : getShortestPaths().getPath(s, t).getEdgeList()) {
						
						model.addEdge(graph2.getEdgeSource(e), graph2.getEdgeTarget(e), String.valueOf(graph2.getEdgeWeight(e)), "dashed=1;strokeWidth=2;strokeColor="+colMap.get(count)+";editable=0;", setVisible);
						matchedCost+=graph2.getEdgeWeight(e);
					}
					++count;
				}else {
					int index = (int) ((int) count - Math.round(Math.random()*colMap.size()));
					for(MyWeightedEdge e : getShortestPaths().getPath(s, t).getEdgeList()) {
						
						model.addEdge(graph2.getEdgeSource(e), graph2.getEdgeTarget(e), String.valueOf(graph2.getEdgeWeight(e)), "dashed=1;strokeWidth=2;strokeColor="+colMap.get(index)+";editable=0;", setVisible);
						matchedCost+=graph2.getEdgeWeight(e);
					}
					++count;
				}

			return count;
		}
	
	
	public KuhnMunkresMinimalWeightBipartitePerfectMatching<MyVertex, MyWeightedEdge> getMatching(){
		return matching;
	}
	
	 public FloydWarshallShortestPaths<MyVertex,MyWeightedEdge> getShortestPaths(){
		return shortestPath;
		 
	 }
	 
	 public ListenableGraph<MyVertex, MyWeightedEdge> getGraph(){
			return graph;
			 
		 }
	 
	 public HierholzerEulerianCycle<MyVertex, MyWeightedEdge> getCycle(){
		 return cycle;
	 }

	public Set<MyVertex> getNeg() {
		return neg;
	}

	public Set<MyVertex> getPos() {
		return pos;
	}

	/**
	 * @param matching the matching to set
	 */
	public void setMatching(KuhnMunkresMinimalWeightBipartitePerfectMatching<MyVertex, MyWeightedEdge> matching) {
		this.matching = matching;
	}

	/**
	 * @return the oddVerteices
	 */
	public Set<MyVertex> getOddVerteices() {
		return oddVerteices;
	}
	
	/**
	 * @param matchedEdges the matchedEdges to set
	 */
	public void setMatchedUndirectEdges(Set<MyWeightedEdge> matchedEdges) {
		this.matchedUndirectEdges = matchedEdges;
	}
	
	/**
	 * @return the matchedEdges
	 */
	public Set<MyWeightedEdge> getMatchedUndirectEdges() {
		return matchedUndirectEdges;
	}
	
	/**
	 * @return the matchedCost
	 */
	public double getMatchedCost() {
		return matchedCost;
	}

	/**
	 * @param matchedCost the matchedCost to set
	 */
	public void setMatchedCost(double matchedCost) {
		this.matchedCost = matchedCost;
	}

	/**
	 * @return the colMap
	 */
	public HashMap<Integer, String> getColMap() {
		return colMap;
	}

}
