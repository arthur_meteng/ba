package model;

import java.io.Serializable;

public class MyVertex implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String name;
	private int delta;
	
	public MyVertex(String name) {
		this.name = name;
	}

	@Override
	public String toString()
    {
        return name;
    }

	public String getName() {
		return name;
	}

	/**
	 * @return the delta
	 */
	public int getDelta() {
		return delta;
	}

	/**
	 * @param delta the delta to set
	 */
	public void setDelta(int delta) {
		this.delta = delta;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	
}
