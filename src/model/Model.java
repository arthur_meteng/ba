/**
 * 
 */
package model;

import java.io.File;
import java.util.ArrayList;
import java.util.Observable;
import java.util.UUID;


import org.jgrapht.ListenableGraph;
import org.jgrapht.graph.DefaultListenableGraph;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.jgrapht.graph.WeightedMultigraph;


/**
 * @author PaulArthur
 *
 */
public class Model extends Observable{
	
	private ArrayList<MyWeightedEdge> edgeList = new ArrayList<MyWeightedEdge>();
	private ArrayList<MyVertex> vertexList = new ArrayList<MyVertex>();
	private ListenableGraph<MyVertex, MyWeightedEdge> diGraph = new DefaultListenableGraph<>(new DirectedWeightedMultigraph<MyVertex, MyWeightedEdge>(MyWeightedEdge.class));
	private ListenableGraph<MyVertex, MyWeightedEdge> unDiGraph = new DefaultListenableGraph<>(new WeightedMultigraph<MyVertex, MyWeightedEdge>(MyWeightedEdge.class));
	
	private File file;
	private boolean graphIsDirected = true;
	private boolean isMultigraph = false;
	  /**
     * A new vertex should be added to the graph
     */
    private static final String ADD_EDGE = "2";
    /**
     * A new vertex should be added to the graph
     */
    private static final String ADD_VERTEX = "1";
    /**
     * The graph shold be reset to his empty value
     */
    private static final String RESET_ALL = "0";
    /**
     * A vertex or Edge should be deleted from the graph
     */
    private static final String DELETE = "3";
	
	/**
	 * Reset the listenable graph to an empty one and clear the maps/lists.
	 * @param view
	 */
	public void resetAll() {
		//clear the datastructures
    	edgeList.clear();
    	vertexList.clear();
    	if(graphIsDirected) {
    		//reset the listenable graph to an empty one
        	setDiGraph( new DefaultListenableGraph<>(new DirectedWeightedMultigraph<MyVertex, MyWeightedEdge>(MyWeightedEdge.class)));
    	} else setUnDiGraph( new DefaultListenableGraph<>(new WeightedMultigraph<MyVertex, MyWeightedEdge>(MyWeightedEdge.class)));
    	
    	ArrayList<Object> a = new ArrayList<>();
		a.add(RESET_ALL);
		
    	setChanged();
        notifyObservers(a);
	
	}
	
	/**
	 * Add a vertex to the graph
	 * @param vertex
	 */
	public void addVertex(MyVertex vertex) {
		
		vertexList.add(vertex);
		if(graphIsDirected) {
			diGraph.addVertex(vertex);
    	} else {
    		unDiGraph.addVertex(vertex);	
    	}
		
		ArrayList<Object> a = new ArrayList<>();
		a.add(ADD_VERTEX);a.add(vertex.getName());
		
		setChanged(); 
        notifyObservers(a);
	}
	
	/**Delete vertex
	 * @param vertex
	 */
	public void deleteVertex(MyVertex vertex) {
		vertexList.remove(vertex);
	    if (graphIsDirected) {
		    diGraph.removeVertex(vertex);
	    }else {
		    unDiGraph.removeVertex(vertex);
	    }
	      
	    ArrayList<Object> a = new ArrayList<>();
		a.add(DELETE);a.add(vertex.getName());
			
		setChanged(); 
	    notifyObservers(a);
			
	}
	
	/**Create and add edge to the graph
	 * @param source
	 * @param target
	 * @param weight
	 */
	public void addEdge(MyVertex source, MyVertex target, Double weight) {
		
		String edgeId = UUID.randomUUID().toString();
		MyWeightedEdge edge = null;
		if(graphIsDirected) {
			edge = diGraph.addEdge(source, target);
			diGraph.setEdgeWeight(edge, weight);
			
    	} else {

    		edge = unDiGraph.addEdge(source, target);
    		unDiGraph.setEdgeWeight(edge, weight);
    	}
		
		edge.setID(edgeId);
		edge.setDirected(graphIsDirected);
		edgeList.add(edge);
		
		ArrayList<Object> a = new ArrayList<>();
		a.add(ADD_EDGE);a.add(edge);
		setChanged(); 
        notifyObservers(a);
	}
	
	
	/**Delete Edge
	 * @param edge
	 */
	public void deleteEdge(MyWeightedEdge edge) {
		edgeList.remove(edge);
	    if (graphIsDirected) {
	    	diGraph.removeEdge(edge);
	    }else {
			unDiGraph.removeEdge(edge);
	    }
	    
	    ArrayList<Object> a = new ArrayList<>();
		a.add(DELETE);a.add(edge.getID());
		setChanged(); 
        notifyObservers(a);
			
	}
	
		
	/**
	 * Add an edge with the given style and weight(label) to the graph.
	 * We assume here that both source and target vertex already exist in the graph.
	 * @param source
	 * @param target
	 * @param label
	 * @param style
	 * @param setVisible 
	 */
	public String addEdge(MyVertex source, MyVertex target, String label, String style, boolean visible) {
		String edgeId = "";
		if(!style.equals("new")) {//die Wert von Style is "new" wenn w�hrend des Eulertour Bildung neue Kanten zu dem vorherrigen Graph hinzugef�gt werden
			edgeId = UUID.randomUUID().toString();
		}else {
			style = "";
			edgeId = "NEW"+UUID.randomUUID().toString();
		}
		Double weight = Double.parseDouble(label);
		MyWeightedEdge edge = null;
		
		if(graphIsDirected) {
			edge = diGraph.addEdge(source, target);
			diGraph.setEdgeWeight(edge, weight);
			
    	} else {
    		
    		edge = unDiGraph.addEdge(source, target);
    		unDiGraph.setEdgeWeight(edge, weight);
    	}
		
		edge.setID(edgeId);
		edge.setDirected(graphIsDirected);
		edgeList.add(edge);
		
		ArrayList<Object> a = new ArrayList<>();
		a.add(ADD_EDGE);a.add(edge);a.add(style);a.add(visible);

		setChanged();
        notifyObservers(a);

		return edgeId;
	}		
	
	/**
	 * Look for the vertex with the same name in the vertex's list.
	 * @param name
	 * @return The vertex if it is in the list or null if not.
	 */
	public MyVertex getVertexByName(String name) {
		for(MyVertex vertex: vertexList) {
			if(name.equals(vertex.toString())) {
				return vertex;
			}
		}
		return null;	
	}
	
	/**
	 * Look for the edge with the same ID in the edge's list.
	 * @param ID
	 * @return The weighted edge if it is in the list or null if not.
	 */
	public MyWeightedEdge getEdgeByID(String ID) {
		for(MyWeightedEdge edge: edgeList) {
			if(ID.equals(edge.getID())) {
				return edge;
			}
		}
		return null;	
	}
	
	/**
	 * Look for the edge with the same source and target vertices as given in the edge's list.
	 * @param source
	 * @param target
	 * @return The weighted edge if it is in the list or null if not.
	 */
	public MyWeightedEdge getEdgeByST(String source, String target) {
		if(graphIsDirected) {
			for(MyWeightedEdge edge: diGraph.edgeSet()) {
				if(source.equals(diGraph.getEdgeSource(edge).getName()) && target.equals(diGraph.getEdgeTarget(edge).getName())) {
					return edge;
				}
			}
		}else {
			for(MyWeightedEdge edge: unDiGraph.edgeSet()) {
				if(source.equals(unDiGraph.getEdgeSource(edge).getName()) && target.equals(unDiGraph.getEdgeTarget(edge).getName())) {
					return edge;
				}else if(target.equals(unDiGraph.getEdgeSource(edge).getName()) && source.equals(unDiGraph.getEdgeTarget(edge).getName())) {
					return edge;
				}
			}
		}
		
		return null;	
	}
	
	/**
	 * @return the edgeList
	 */
	public ArrayList<MyWeightedEdge> getEdgeList() {
		return edgeList;
	}

	/**
	 * @return the vertexList
	 */
	public ArrayList<MyVertex> getVertexList() {
		return vertexList;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}
	
	/**
	 * @return the diGraph
	 */
	public ListenableGraph<MyVertex, MyWeightedEdge> getDiGraph() {
		return diGraph;
	}

	
	/**
	 * @param diGraph the diGraph to set
	 */
	public void setDiGraph(ListenableGraph<MyVertex, MyWeightedEdge> diGraph) {
		this.diGraph = diGraph;
	}

	
	/**
	 * @return the unDiGraph
	 */
	public ListenableGraph<MyVertex, MyWeightedEdge> getUnDiGraph() {
		return unDiGraph;
	}

	
	/**
	 * @param unDiGraph the unDiGraph to set
	 */
	public void setUnDiGraph(ListenableGraph<MyVertex, MyWeightedEdge> unDiGraph) {
		this.unDiGraph = unDiGraph;
	}
	
	/**
	 * @return the graphIsDiredted
	 */
	public boolean graphIsDirected() {
		return graphIsDirected;
	}
	
	/**
	 * @param graphIsDiredted the graphIsDiredted to set
	 */
	public void setGraphIsDiredted(boolean graphIsDiredted) {
		this.graphIsDirected = graphIsDiredted;
	}
	
	/**
	 * @return the isMultigraph
	 */
	public boolean isMultiGraph() {
		return isMultigraph;
	}
	
	/**
	 * @param isMultigraph the isMultigraph to set
	 */
	public void setIsMultiGraph(boolean isMultigraph) {
		this.isMultigraph = isMultigraph;
	}

}
