package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


/**
* Creates graphs from a csv file.
*
* @param <MyVertex> Vertex
* @param <MyWeightedEdge> Edge
*
*/
public class GraphLoader {

   /**
    * model used to update the intern datastructure of graph as soon as a new one is created
    */
   private Model model;
   /**
    * The csv uses a comma delimiter.
    */
   private static final String COMMA_DELIMITER = ",";
   /**
    * The source vertex index
    */
   private static final int SOURCE_VERTEX_INDEX = 0;
   /**
    * The target vertex index
    */
   private static final int TARGET_VERTEX_INDEX = 1;
   /**
    * The weight index
    */
   private static final int WEIGHT_INDEX = 2;
   
   /**
    * Initializes a new {@link GraphLoader}.
    * @param model
    */
   public GraphLoader(Model model) {
       this.model = model;
       
   }

   /**
    * Loads a new graph from a csv file
    * 
    * @throws FileNotFoundException
    * @throws NoSuchMethodException
    */
   public void loadGraph(File csvFile) throws FileNotFoundException,IOException{

       BufferedReader fReader = null;
       try {
       //create the file reader  
		fReader = new BufferedReader(new FileReader(csvFile));
		
       //read the CSV file Header to skip it
		fReader.readLine();

       // Load the edges from the input file.
       loadEdges(fReader, csvFile);
       }catch(Exception e) {
       	e.printStackTrace();
       }finally {
       	fReader.close();
       	
       }
       

   }

   /**
    * Loads all edges into the graph.
    *
    * @param fReader The scanner that will parse the csv file.
 * @param csvFile 
    *
    * @return The graph.
    * @throws IOException 
    */
   private void loadEdges(BufferedReader fReader, File csvFile) throws IOException {
	   String line = "";
	   //reset the model to an empty one
	   model.resetAll();
	   	
	   // Go through the file and add each edge.
	   while ((line = fReader.readLine()) != null) {
   	
	       // Split the line stating from the second line
	       String[] row = line.split(COMMA_DELIMITER);
	       loadEdge(row); 
	   }
   }

   /**
    * Loads an edge into the graph.
    *
    * @param row     The row from which to load the edge.
    * @param view
    * @param model
    *
    */
   public  void loadEdge (String[] row) {
   		try {
   			String tVertexName = row[TARGET_VERTEX_INDEX];
	        String sVertexName = row[SOURCE_VERTEX_INDEX];
	        MyVertex tVertex = new MyVertex(tVertexName);
	        MyVertex sVertex = new MyVertex(sVertexName);
	        double weight = Double.parseDouble(row[WEIGHT_INDEX]);
	        
	        if(model.getVertexByName(sVertexName)==null) {
	        	model.addVertex(sVertex);
	        }
	       
	        if(model.getVertexByName(tVertexName)==null) {
	        	model.addVertex(tVertex);
	        }

			model.addEdge(model.getVertexByName(sVertexName), model.getVertexByName(tVertexName),weight);
			
		}catch(Exception e) {
			e.printStackTrace();
		}

   }
   
}
