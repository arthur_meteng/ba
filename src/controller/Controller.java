/**
 * 
 */
package controller;

import java.awt.Desktop;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.layout.mxCompactTreeLayout;
import com.mxgraph.layout.mxEdgeLabelLayout;
import com.mxgraph.layout.mxFastOrganicLayout;
import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.layout.mxOrganicLayout;
import com.mxgraph.layout.mxParallelEdgeLayout;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource.mxIEventListener;
import com.mxgraph.util.mxRectangle;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxGraphSelectionModel;

import View.AboutView;
import View.LexiconView;
import View.MainView;
import View.TutorialView;
import model.Algorithms;
import model.GraphLoader;
import model.GraphSaver;
import model.Model;
import model.MyVertex;
import model.MyWeightedEdge;

/**
 * @author PaulArthur
 *
 */
public class Controller implements FocusListener,ActionListener, mxIEventListener {

	private MainView view;
	private Model model;
	private Algorithms cpp;
	private Map<String, Integer> colourMap;
	private Map<Integer, String> colMap;
	private GraphSaver saver;
	private final String redColor = "#FF0000";
	private final String lightRedColor = "#f41a1a";
	private final String blueColor = "#0000ff";
	private final String defaultFillColor = "#C3D9FF";
	private final String defaultStrokeColor = "#6482B9";
	private final String defaultFontColor = "#446299";
	private final String greenColor = "#38F000";
	private final String lightGreenColor = "#98F07E";
	private final String purpleColor = "#800080";
	private final String yellowColor = "#ffd700";
	private final String marronColor = "#900102";
	private final String wColor = "#006666";
	private final String darkredColor = "#8B0000";
	private String previousEdgeColor = blueColor;
	private final BufferedImage Smiling_With_Sweat_Emoji =load("Smiling_With_Sweat_Emoji_2.png");
	private final BufferedImage Neutral_Emoji = load("Neutral_Emoji_icon.png");
	private final BufferedImage smiling_Emoji = load("Emoji_Icon_-_Smiling.png");
	private final BufferedImage Heart_Eyes_Emoji = load("Heart_Eyes_Emoji_2.png");
	private final BufferedImage crying_Emoji = load("Crying_Emoji_Icon.png");
	private final BufferedImage nerd_Emoji = load("Nerd_Emoji_Icon.png");
	private final BufferedImage wink_Emoji = load("Wink_Emoji_2.png");
	private final BufferedImage think_Emoji = load("Emoji_Icon_-_Thinking.png");
	private final BufferedImage postboten = load("Postboten.png");

	private final ImageIcon smilingIcon = new ImageIcon(smiling_Emoji);
	private Image image2 = smilingIcon.getImage().getScaledInstance(48,48,0);
	private boolean matchedEdgeAdded = false;
	private boolean eulerTourCanStart = false;
	private boolean startVertexSelected = false;
	private boolean withoutMatchedEdges = false;
	private ArrayList<String> subtour = new ArrayList<String>();
	private ArrayList<mxCell> cellsToDelete = new ArrayList<>();
	private ArrayList<mxCell> subtourEdges = new ArrayList<mxCell>();
	/**
	 * for coloring edges
	 */
	private int count = 0;
	/**
	 * actual cost of the eulerian cycle
	 */
	private double actualCost = 0;
	/**
	 * expected total cost of the eulerian cycle
	 */
	private double totalCost = 0;
	/**Creates an instance of the Controller
	 * @param model 
	 * @param view 
	 * 
	 */
	public Controller(MainView view, Model model) {
		
		this.model = model;
		this.view = view;
		this.view.addCancelLastMoveListener(this);
		this.view.addResetListener(this);
		this.view.addCellSelectionListener(this);
		this.view.addFocusListener(this);
		this.view.addHelpItemListener(this);
		this.view.addOkListener(this);
		this.view.addTutorialItemListener(this);
		this.view.addEdgeListener(this);
		this.view.addStartListener(this);
		this.view.addVertexListener(this);
		this.view.addDeleteListener(this);
		this.view.addExitItemListener(this);
		this.view.addSaveAsItemListener(this);
		this.view.addSaveItemListener(this);
		this.view.addLoadItemListener(this);
		this.view.addNewItemListener(this);
		this.view.addCycleListener(this);
		this.view.addhierListener(this);
		this.view.addParEdgeListener(this);
		this.view.addOrganicListener(this);
		this.view.addFastOListener(this);
		this.view.addAboutItemListener(this);
		colourMap = new HashMap<String, Integer>();
		colMap = new HashMap<Integer, String>();
		colourMap.put(blueColor, 0);colMap.put(0, blueColor);
		colourMap.put(yellowColor, 1);colMap.put(1, yellowColor);
		colourMap.put(purpleColor, 2);colMap.put(2, purpleColor);
		colourMap.put(lightGreenColor, 3);colMap.put(3, lightGreenColor);
		colourMap.put(redColor, 4);colMap.put(4, redColor);
		colourMap.put(marronColor, 5);colMap.put(5, marronColor);
		colourMap.put(lightRedColor, 6);colMap.put(6, lightRedColor);
		colourMap.put(wColor, 7);colMap.put(7, wColor);
		colourMap.put(darkredColor, 8);colMap.put(8, darkredColor);
		
		Object[] options = {"Directed Graph", "Undirected Graph"};
		int n = displayOptionMessage("Welcome to the Chinese Postman Game!!!\n\nMy Name is Mr. Ho,\nI am the postman responsable for any mail delivery around the city.\nI will need your help to find the shortest distance for me to cover.\n\nPlease select the type of graph you want to proceed with.\n",postboten, options);
		if(n==0) {
			this.model.setGraphIsDiredted(true);
		}else this.model.setGraphIsDiredted(false);
	}

	@Override
	public void focusGained(FocusEvent e) {
		if(e.getSource().equals(view.getTextField())) {
			view.getTextField().setText("");
		}
		
	}

	@Override
	public void focusLost(FocusEvent e) {
		
	}
	
	@Override
	public void invoke(Object sender, mxEventObject evt) {
		mxGraph graph = view.getComponent().getGraph();
		
		if (sender instanceof mxGraphSelectionModel) {
			Object[] o = ((mxGraphSelectionModel)sender).getCells();
			if(o.length!=0) {
				selectCell(o, graph);
			}else {
				int count=cellsToDelete.size();
				if(!cellsToDelete.isEmpty()) {
					System.out.println("List:"+cellsToDelete.toString());
					for (int i = 0;i<count;i=0) {
						System.out.println("Cell:"+(mxCell)(cellsToDelete.get(i)));
						mxCell cell = ((mxCell)cellsToDelete.get(i));
						if(cell.isVertex()) {
							graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, defaultFillColor, new Object[] {cellsToDelete.get(i)});
							cellsToDelete.remove(cell);
						}else if (cell.isEdge()) {
							graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, defaultStrokeColor, new Object[] {cellsToDelete.get(i)});
							cellsToDelete.remove(cell);
						}
						--count;
					}	
				}
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource().equals(view.getButtonDel())) {
			deleteCell();
		
		}else if(e.getSource().equals(view.getButtonAdd())) {
			if(!view.getTextField().hasFocus()) {
				addVertexFromUser (new MyVertex(view.getTextField().getText()));
				
			}
			
		}else if(e.getSource().equals(view.getAboutItem())) {
			new AboutView();
			
		}else if(e.getSource().equals(view.getHelpItem())) {
			new LexiconView();
			
		}else if(e.getSource().equals(view.getTutorialItem())) {
			new TutorialView();
			
			
		}else if(e.getSource().equals(view.getButtonEdge())) {
			if(model.graphIsDirected()) {
				addEdgeFromUser("Enter the name of the Source Vertex", "Enter the name of the Target Vertex");
			} else addEdgeFromUser("Enter the name of the first Vertex", "Enter the name of the Second Vertex");
		
		}else if(e.getSource().equals(view.getButtonOk())) {
			if(!matchedEdgeAdded) {
				if(model.graphIsDirected()) {
				cpp.addMatchedEdgeForDirectedGraph(true);
				String tour = "";
				String [] matchs = cpp.getMatchingEdgesForDiGraph().split(",");
				int count = 0;
				for(int i = 0; i<matchs.length;i++) {
					
					if(matchs[i].startsWith("[")) {
						tour = tour.concat("<font face = \"Arial\" size = \"4\" color=\""+cpp.getColMap().get(count)+"\">"+matchs[i].substring(1)+"</font>, ");
					}else if(matchs[i].endsWith("]")){
						tour = tour.concat("<font face = \"Arial\" size = \"4\" color=\""+cpp.getColMap().get(count)+"\">"+matchs[i].substring(0, matchs[i].length()-1)+"</font>");
					}else {
						tour = tour.concat("<font face = \"Arial\" size = \"4\" color=\""+cpp.getColMap().get(count)+"\">"+matchs[i]+"</font>, ");
					}
					++count;
				}
				
				view.getTextPane().setText(view.getTitleOfTextArea()+"Here is(are) the path(s) that have been added :<br>["+tour+"]<br><br>Click on the <b>OK</b> button below to proceed.");
				
				}else {
					cpp.addMatchedEdgeForUnDirectedGraph(true);
					String tour = "";
					String[] matchs = cpp.getMatchedUndirectEdges().toString().split(",");
					int count = 0;
					for(int i = 0; i<matchs.length;i++) {
						if(matchs[i].startsWith("[")) {
							tour = tour.concat("<font face = \"Arial\" size = \"4\" color=\""+cpp.getColMap().get(count)+"\">"+matchs[i].substring(1)+"</font>, ");
						}else if(matchs[i].endsWith("]")){
							tour = tour.concat("<font face = \"Arial\" size = \"4\" color=\""+cpp.getColMap().get(count)+"\">"+matchs[i].substring(0, matchs[i].length()-1)+"</font>");
						}else {
							tour = tour.concat("<font face = \"Arial\" size = \"4\" color=\""+cpp.getColMap().get(count)+"\">"+matchs[i]+"</font>, ");
						}
						++count;		
					}
					view.getTextPane().setText(view.getTitleOfTextArea()+"Here is(are) the path(s) that have been added :<br>["+tour+"]<br><br>Click on the <b>OK</b> button below to proceed.");

				}
				createLayout("circleLayout").execute(view.getGraph().getDefaultParent());
				createLayout("parallelEdges").execute(view.getGraph().getDefaultParent());
				
				//Save the actual graph so that one can get it while pressing the reset button
				try {
					File file = new File("toReset.csv");
					PrintWriter pw = new PrintWriter(file);
					pw.close();
					saver = new GraphSaver(model);
					saver.saveGraph(file.getAbsolutePath());
				} catch (NoSuchMethodException | FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				matchedEdgeAdded=true;
			}else {
				Object[] o = view.getGraph().getChildCells(view.getGraph().getDefaultParent());
				//reset the total cost
				totalCost = 0;
				//set the matched edge with the default style
				for (int i = 0;i<o.length;i++) {
					
					if(((mxCell)o[i]).isEdge()){
						totalCost+=Double.parseDouble(view.getGraph().getLabel((mxCell)o[i]));
						if ((String.valueOf(view.getGraph().getCellStyle(o[i]).get(mxConstants.STYLE_DASHED))).equals("1")) {
							view.getComponent().getGraph().setCellStyles(mxConstants.STYLE_STROKECOLOR, defaultStrokeColor, new Object[] {o[i]});
							view.getComponent().getGraph().setCellStyles(mxConstants.STYLE_FONTCOLOR, defaultFontColor, new Object[] {o[i]});
							view.getComponent().getGraph().setCellStyles(mxConstants.STYLE_DASHED, "0", new Object[] {o[i]});
							view.getComponent().getGraph().setCellStyles(mxConstants.STYLE_STROKEWIDTH, "1", new Object[] {o[i]});
						}
					}	
				}
				eulerTourCanStart = true;
				displayInfoMessage("We now have an eulerian graph.\nLet's try to build the eulerian cycle.", Heart_Eyes_Emoji);
				showEulerianCycle();
				view.addAlgoButtons();
				view.getButtonOk().setEnabled(false);
			}
			
		}else if(e.getSource().equals(view.getCicle())) {
			createLayout("circleLayout").execute(view.getGraph().getDefaultParent());

		}else if(e.getSource().equals(view.getFastOrganic())) {
			createLayout("fastOrganicLayout").execute(view.getGraph().getDefaultParent());

		}else if(e.getSource().equals(view.getOrganic())) {
			createLayout("organicLayout").execute(view.getGraph().getDefaultParent());

		}else if(e.getSource().equals(view.getHierarchical())) {
			createLayout("horizontalHierarchical").execute(view.getGraph().getDefaultParent());

		}else if(e.getSource().equals(view.getParallelEdge())) {
			createLayout("parallelEdges").execute(view.getGraph().getDefaultParent());
	
		}else if(e.getSource().equals(view.getLoadItem())) {
			
			loadGraph();
			
		}else if(e.getSource().equals(view.getSaveItem())) {
			if(model.getFile() != null) {
				saveGraphInActualFile();
				
	         }else {
	        	 saveGraphInNewFile();
	         }
				
		}else if(e.getSource().equals(view.getSaveAsItem())) {
			saveGraphInNewFile();
			
		}else if(e.getSource().equals(view.getExitItem())) {
			System.exit(0);	
			
		}else if(e.getSource().equals(view.getNewItem())) {

	    	if(cpp!=null) {
				matchedEdgeAdded=false;		
				eulerTourCanStart = false;
				startVertexSelected = false;
				withoutMatchedEdges = false;
				totalCost = 0;
				cpp.setMatchedCost(0);
				actualCost = 0;
				subtour.clear();
				subtourEdges.clear();
			}
			Object[] options = {"Directed Graph", "Undirected Graph"};
			int n = displayOptionMessage("OOH! There is a new commission for me.\nPlease select the type of graph you want to proceed with.",postboten, options);
			view.addGraphButtons();
			if(n==0) {
				this.model.setGraphIsDiredted(true);
			}else this.model.setGraphIsDiredted(false);
			//reset the datastructure in the model
			model.resetAll();
			//reset the layout of the view
			resetLayoutView();
		}else if(e.getSource().equals(view.getButtonStart())) {
			
			startGame(model.graphIsDirected());

		}else if(e.getSource().equals(view.getButtonReset())) {
			
			resetGraph();

		}else if(e.getSource().equals(view.getButtonBackOffLastMove())) {
			
			resetLastMove(view.getGraph());

		}
	}
	
	/**
	 * save the Graph in a new file created by the user. 
	 * The user can choose the directory where he wants to save his file 
	 */
	public void saveGraphInNewFile() {
		 
		JFileChooser chooser = new JFileChooser();
	    try {
			chooser.setCurrentDirectory(new File(getClass().getProtectionDomain().getCodeSource().getLocation().toURI()));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    int retrival = chooser.showSaveDialog(null);
	    if (retrival == JFileChooser.APPROVE_OPTION) {
	        try {
	        	File f = chooser.getSelectedFile();
	       	 	String filePath = f.getAbsolutePath();
	       	 	if(!filePath.endsWith(".csv")) {
	       	 		f = new File(filePath + ".csv");
		            model.setFile(f);
		            GraphSaver saver = new GraphSaver(model);
	
		            saver.saveGraph(model.getFile().getAbsolutePath());
	       	 	}else {
	       	 		model.setFile(f);
		            GraphSaver saver = new GraphSaver(model);
		            saver.saveGraph(model.getFile().getAbsolutePath());
	       	 	}
	       	 	displayInfoMessage("Graph saved!!!", smiling_Emoji);
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	    }	
	}
	
	/**
	 * save the Graph in the actual file created by the user. 
	 */
	public void saveGraphInActualFile() {
		GraphSaver saver = new GraphSaver(model);
		try {
			saver.saveGraph(model.getFile().getAbsolutePath());
			displayInfoMessage("Graph saved!!!", smiling_Emoji);
		} catch (NoSuchMethodException ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * Loads the graph from the file the user chooses in the view.
	 */
	public void loadGraph() {
		
		
		GraphLoader loader = new GraphLoader(model);
        
		// set up the file chooser
//		 String cwd = System.getProperty("user.dir");
//	     final JFileChooser jfc = new JFileChooser(cwd);
		final JFileChooser jfc = new JFileChooser();
		try {
			jfc.setCurrentDirectory(new File(getClass().getProtectionDomain().getCodeSource().getLocation().toURI()));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     
	     if (jfc.showOpenDialog(view) != JFileChooser.APPROVE_OPTION) {
	    	 return;
	     }else {
	    	
	         model.setFile(jfc.getSelectedFile());
             try {
				loader.loadGraph(model.getFile());
			 } catch (IOException ex) {
				ex.printStackTrace();
			 }
	     }  
		
		//enable the start button if the graph has at least 2 vertices
		if(view.getGraph().getChildVertices(view.getGraph().getDefaultParent()).length>1) {
			view.getButtonStart().setEnabled(true);
			view.getMenuLayouts().setEnabled(true);
			view.addGraphButtons();
			setGraphButtonsEnabled(true);
			view.getTextPane().setText(view.getTitleOfTextArea()+"You can now press the <b>Start</b> button below to start with algorithm!!!");
			createLayout("circleLayout").execute(view.getGraph().getDefaultParent());
			createLayout("parallelEdges").execute(view.getGraph().getDefaultParent());
		}
		
		if(cpp!=null) {
		matchedEdgeAdded=false;
		eulerTourCanStart = false;
		startVertexSelected = false;
		withoutMatchedEdges = false;
		totalCost = 0;
		count = 0;
		cpp.setMatchedCost(0);
		actualCost = 0;
		subtour.clear();
		subtourEdges.clear();
		}
       
	}
	private void resetLastMove(mxGraph graph) {
		
		if(subtourEdges.size()!=0) {
			mxCell edgeCell = subtourEdges.get(subtourEdges.size()-1);
			mxCell sourceCell = (mxCell) edgeCell.getSource();
			mxCell targetCell = (mxCell) edgeCell.getTarget();
			resetStyle(graph, edgeCell, sourceCell,targetCell);
			actualCost -= Double.parseDouble(graph.getLabel(edgeCell));
			showEulerianCycle();
		}
	}

	/**
	 * Reset the Graph to the one before one started building the eulerian cycle
	 */
	private void resetGraph() {
	
		count = 0;
		actualCost = 0;
		subtour.clear();
		subtourEdges.clear();
		GraphLoader loader = new GraphLoader(model);
		
		try {
			File file = new File("toReset.csv");
			loader.loadGraph(file);
		} catch (IOException e) {
			e.printStackTrace();
		};	
		showEulerianCycle();
		startVertexSelected = false;
		createLayout("circleLayout").execute(view.getGraph().getDefaultParent());
		createLayout("parallelEdges").execute(view.getGraph().getDefaultParent());
	}

	/**
	 * @param vertex
	 * add a new vertex from user
	 */
	private void addVertexFromUser (MyVertex vertex) {

		if(vertex.getName().equals("")|| vertex.getName().equals("write name of vertex")) {
			displayErrorMessage("Please enter a name for the vertex!!!",Neutral_Emoji);
		}else if(vertex.getName()!=null && vertex.getName().length()>10) {
			displayErrorMessage("The name of the Vertex is too big", Smiling_With_Sweat_Emoji);
		}else if(model.getVertexList().contains(model.getVertexByName(vertex.getName()))) {
			displayErrorMessage("Vertex Already exist!!!", Smiling_With_Sweat_Emoji);
			
		}else {
			model.addVertex(vertex);
		}
		//enable the start button as soon as the graph has at least 2 vertices
		if(view.getGraph().getChildVertices(view.getGraph().getDefaultParent()).length>1) {
			view.getButtonStart().setEnabled(true);
			view.getMenuLayouts().setEnabled(true);
		}
	}

	 /**
 	 * add a new edge from user
 	 * @param message1
	 * @param message2
	 * @param style
	 * @param graph
	 */
	private void addEdgeFromUser(String message1,String message2) {
		boolean continu = true;
		if(model.getVertexList().size()>=2) {
			String source = null;
			String target = null;
			MyVertex sourceV = null;
			MyVertex targetV = null;
			try {
				while(true) {
					source = (String) JOptionPane.showInputDialog(null,message1,"Source Vertex", JOptionPane.PLAIN_MESSAGE, new ImageIcon(image2),  null, "").toString();
					sourceV = model.getVertexByName(source);
					if(!source.equals(null)) {
						if(!model.getVertexList().contains(sourceV)) {
							displayErrorMessage("Vertex with the name "+source+" doesn't exist yet!!!", Smiling_With_Sweat_Emoji);
						}else break;
					}else {
						continu = true;
						break;
					}
					
				}
				if(continu) {
					while(true) {
						target = JOptionPane.showInputDialog(null,message2,"Target Vertex", JOptionPane.PLAIN_MESSAGE, new ImageIcon(image2),  null, "").toString();
						targetV = model.getVertexByName(target);
						if(!target.equals(null)) {
							if(!model.getVertexList().contains(targetV)) {
								displayErrorMessage("Vertex with the name "+target+" doesn't exist yet!!!", Smiling_With_Sweat_Emoji);
							}else {
								if(source.equals(target)) {
									displayErrorMessage("Loops are not allowed!!!", Neutral_Emoji);
									continu = false;
								}else break;
							}
						}else {
							continu = false;
							break;
						}
					}
					if(continu) {
						while(true) {
							try {
								String name = JOptionPane.showInputDialog(null,"Enter weight","Weight", JOptionPane.PLAIN_MESSAGE, new ImageIcon(image2),  null, "").toString();
								if(!name.equals(null)) {
									Double weight = Double.parseDouble(name);
									
									model.addEdge(sourceV, targetV,weight);
									
									break;
								}else break;
							}catch (NumberFormatException e) {
								displayErrorMessage("The weight should be a number!!!",Neutral_Emoji);
							}
						}
					}
				}
			}catch(Exception e) {
				
			}
			createLayout("circleLayout").execute(view.getGraph().getDefaultParent());
			createLayout("parallelEdges").execute(view.getGraph().getDefaultParent());
		} else displayErrorMessage("Please enter add at least 2 vertices!!!",Neutral_Emoji);
		
	}
	
	/**
	 * Delete a selected cell 
	 */
	private void deleteCell() {
		if(!cellsToDelete.isEmpty()) {
			for(int i = 0; i<cellsToDelete.size(); i++) {
				if((cellsToDelete.get(i)).isVertex()){ 
					
					model.deleteVertex(model.getVertexByName(String.valueOf((cellsToDelete.get(i)).getValue())));
					
			    }else if((cellsToDelete.get(i)).isEdge()){
			    	MyWeightedEdge edge = model.getEdgeByID((cellsToDelete.get(i).getId()));
				    model.deleteEdge(edge);    
			    }
			
				//Disable the start button as soon as the graph is empty
				if(view.getGraph().getChildVertices(view.getGraph().getDefaultParent()).length<=1) {
					view.getButtonStart().setEnabled(false);
					view.getMenuLayouts().setEnabled(false);
					//TODO reset all the variables as soon as the graph is empty
				}
			}
			cellsToDelete.clear();
		}else {
			displayErrorMessage("There is actually nothing to delete!\nSelect the vertex(ices) or Edge(s) you want to delete first.", Neutral_Emoji);
		}
	}
	
	/**
	 * Start the Game with a directed graph or an indirected graph depending on the value of the parameter.
	 * @param graphIsDirected
	 */
	private void startGame(boolean graphIsDirected) {
		resetVerticesColor();
		if (graphIsDirected) {
			setCpp(new Algorithms(model, model.getDiGraph()));
		}else { 
			setCpp(new Algorithms(model, model.getUnDiGraph()));
		}
			if(cpp.isStronglyConnected()) {
				if(!cpp.hasNegativeCycle()) {
					
					displayInfoMessage("Your graph is strongly connected and doesn't have negative cycle.", Heart_Eyes_Emoji);
					displayInfoMessage("Now Look at your graph more closely and answer the following question.", nerd_Emoji);
					int n = displayQuestionMessage("Is your graph eulerian?",think_Emoji);
					
					int countEdges = 0;
					Object[] obs = view.getGraph().getChildCells(view.getGraph().getDefaultParent());
					if(model.graphIsDirected()) {
						for(Object ob1 : obs) {
							for(Object ob2 : obs) {
								if(!(ob1.equals(ob2))&&(((mxCell)ob1)).isVertex()&&(((mxCell)ob2)).isVertex()) {
									Object[] s = view.getGraph().getEdgesBetween(ob1, ob2, true);
									
									for(int f = 0;f<s.length;f++) {
										mxCell c = (mxCell) s[f];
										if(c.isVisible()) {
											countEdges++;
										}
									}
									if(countEdges >= 2) {
										model.setIsMultiGraph(true);
										break;
									}
								}
							}
						}
					}else {
						
						for(Object ob : obs) {
							for(Object ob2 : obs) {
								if(!(ob.equals(ob2))&&(((mxCell)ob)).isVertex()&&(((mxCell)ob2)).isVertex()) {
									Object[] s = view.getGraph().getEdgesBetween(ob, ob2);
									
									for(int f = 0;f<s.length;f++) {
										mxCell c = (mxCell) s[f];
										if(c.isVisible()) {
											countEdges++;
										}
									}
									if(countEdges >= 2) {
										model.setIsMultiGraph(true);
										break;
									}
								}
							}
						
						}
					}
					if(cpp.isEulerian() && n==0) {
						displayInfoMessage("You have it right.\nLet's now build the eulerian cycle.", smiling_Emoji);
						//Save the actual graph so that one can get it while pressing the reset button
						try {
							
							File file = new File("toReset.csv");
					        PrintWriter pw = new PrintWriter(file);
					        pw.close();
							saver = new GraphSaver(model);
							saver.saveGraph(file.getAbsolutePath());
							
						} catch (NoSuchMethodException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						withoutMatchedEdges = true;
						Object[] o = view.getGraph().getChildCells(view.getGraph().getDefaultParent());
						if(model.graphIsDirected()) {
							//compute matching
							cpp.setHungarian();
							//add matched edges
							cpp.addMatchedEdgeForDirectedGraph(false);
						}else {
							cpp.getMatchingEdgesForUnDiGraph();
							cpp.addMatchedEdgeForUnDirectedGraph(false);
						}
						//reset the total cost
						totalCost = 0;
						//compute the total cost
						for (int i = 0;i<o.length;i++) {
							
							if(((mxCell)o[i]).isEdge()){
								totalCost+=Double.parseDouble(view.getGraph().getLabel((mxCell)o[i]));
							}	
						}
						view.getButtonStart().setEnabled(false);
						setGraphButtonsEnabled(false);
						view.addAlgoButtons();
						totalCost+=cpp.getMatchedCost();
						eulerTourCanStart = true;
						showEulerianCycle();
					}else if ((cpp.isEulerian() && n==1)||((!cpp.isEulerian()) && n==0)){
						displayInfoMessage("It's unfortunatly incorrect.\nLet's get some help from Wikipedia. You can also check the definition of \"eulerian\" in the lexicon provided in the \"Help\" Menu.", Smiling_With_Sweat_Emoji);
						try {
							openWebpage(new URL("https://en.wikipedia.org/wiki/Eulerian_path"));
						} catch (MalformedURLException e) {
							e.printStackTrace();
						}
					}else if ((!cpp.isEulerian()) && n==1){
						Object[] options = {"Add Edges first", "Make a try"};
						int t = displayOptionMessage("You have it right!!!\nWe will indeed need to add some more Edges to make the graph become eulerian.\nBut you can still try to build an eulerian cycle with the graph as it is now.",smiling_Emoji, options);
						//if add edges first is selected
						if(t == 0) {
							if(graphIsDirected) {
								//TODO create a new view
								cpp.setHungarian();
								view.getTextPane().setText(view.getTitleOfTextArea() + "Here is(are) the path(s) to be added :<br>" + cpp.getMatchingEdgesForDiGraph() + "<br><br>Click on the <b>OK</b> button below to add those edges to the graph.");
							}else {
								if(cpp.getMatchingEdgesForUnDiGraph()!=null) {
									view.getTextPane().setText(view.getTitleOfTextArea()+"Here is(are) the path(s) to be added :<br>" + cpp.getMatchedUndirectEdges().toString() + "<br><br>Click on the <b>OK</b> button below to add those edges to the graph.");
								}
							}
							view.getButtonOk().setEnabled(true);
							view.getButtonStart().setEnabled(false);
							setGraphButtonsEnabled(false);
						//if make a try is selected
						}else if(t == 1) {
							//Save the actual graph so that one can get it while pressing the reset button
							try {
								
								File file = new File("toReset.csv");
						        PrintWriter pw = new PrintWriter(file);
						        pw.close();
								saver = new GraphSaver(model);
								saver.saveGraph(file.getAbsolutePath());
								
							} catch (NoSuchMethodException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							withoutMatchedEdges = true;
							Object[] o = view.getGraph().getChildCells(view.getGraph().getDefaultParent());
							if(model.graphIsDirected()) {
								//compute matching
								cpp.setHungarian();
								//add matched edges
								cpp.addMatchedEdgeForDirectedGraph(false);
							}else {
								cpp.getMatchingEdgesForUnDiGraph();
								cpp.addMatchedEdgeForUnDirectedGraph(false);
							}
							//reset the total cost
							totalCost = 0;
							//compute the total cost
							for (int i = 0;i<o.length;i++) {
								
								if(((mxCell)o[i]).isEdge()){
									totalCost+=Double.parseDouble(view.getGraph().getLabel((mxCell)o[i]));
								}	
							}
							view.getButtonStart().setEnabled(false);
							setGraphButtonsEnabled(false);
							view.addAlgoButtons();
							totalCost+=cpp.getMatchedCost();
							eulerTourCanStart = true;
							showEulerianCycle();
						}
						
					}
					
				}else {
					displayErrorMessage("The graph has a negativ cycle!!!",crying_Emoji);
					//color the vertex with negative cycle in red
					String name = cpp.getNegativeCostVertex();
					if(name!=null) {
						Object[] cells = view.getGraph().getChildCells(view.getGraph().getDefaultParent());
		            	
						for(int k = 0; k<cells.length; k++) {
							mxCell cell = (mxCell)cells[k];
							if(cell.isVertex()&& cell.getValue().equals(name)) {
								
								view.getComponent().getGraph().setCellStyles(mxConstants.STYLE_FILLCOLOR, redColor, new Object[] {cells[k]});
								view.getTextPane().setText(view.getTitleOfTextArea()+"The vertex <b>"+name+"</b> for example has a path to itself with a negative total weight. This means there is at least one negative cycle in the graph.<br>Please check the weights of your edges and change where necessary.");
							}
						}
					}
				}
			}else {
				displayErrorMessage("The graph is not strongly connected!!!",crying_Emoji);
				//color the disconnected vertices in red
				String v1 = "";
				String v2 = "";
			
				for(int i = 0; i<cpp.getDisconnectedVertices().size();i++) {
					String vertexName = cpp.getDisconnectedVertices().get(i);
					Object vertexCell = null;
					Object[] cells = view.getGraph().getChildCells(view.getGraph().getDefaultParent());
	            	
					for(int k = 0; k<cells.length; k++) {
						mxCell cell = (mxCell)cells[k];
						if(cell.isVertex()&& cell.getValue().equals(vertexName)) {
							vertexCell = cells[k];
						}
					}
					if(vertexCell!=null) {	
						view.getComponent().getGraph().setCellStyles(mxConstants.STYLE_FILLCOLOR, redColor, new Object[] {vertexCell});
						if(i<cpp.getDisconnectedVertices().size()-1) {
							v1 = cpp.getDisconnectedVertices().get(i);
							v2 = cpp.getDisconnectedVertices().get(i+1);
						}
					}
				}
				view.getTextPane().setText(view.getTitleOfTextArea()+"There is for example no path between <b>"+v1+"</b> and <b>"+v2+"</b>; "+"<br>Try to add some edges to connect them.");
			}
	}
	
	/**
	 * Resets the color of all vertices to the default one.
	 */
	private void resetVerticesColor() {
		Object[] cells = view.getGraph().getChildCells(view.getGraph().getDefaultParent());
    	
		for(int k = 0; k<cells.length; k++) {
			mxCell cell = (mxCell)cells[k];
			if(cell.isVertex()) {
				view.getComponent().getGraph().setCellStyles(mxConstants.STYLE_FILLCOLOR, defaultFillColor, new Object[] {cells[k]});
			}
		}	
	}
	
	/**
	 * Creates a layout according to the given name.
	 * @param name
	 * @return layout
	 */
	private mxIGraphLayout createLayout(String name) {
		  mxIGraphLayout layout = null;

		  if (name != null) {
		    mxGraph graph = view.getComponent().getGraph();

		    if (name.equals("verticalHierarchical")) {
		      layout = new mxHierarchicalLayout(graph){
			        
		    	    /**
			         * Overrides the empty implementation to return the size of the graph control.
			         */
			        public mxRectangle getContainerSize() {
			          return view.getComponent().getLayoutAreaSize();
			        }
			      };
			      
		    } else if (name.equals("horizontalHierarchical")) {
		      layout = new mxHierarchicalLayout(graph, JLabel.WEST);
		      
		    } else if (name.equals("verticalTree")) {
		      layout = new mxCompactTreeLayout(graph, false);
		      
		    } else if (name.equals("horizontalTree")) {
		      layout = new mxCompactTreeLayout(graph, true);
		      
		    } else if (name.equals("parallelEdges")) {
		      layout = new mxParallelEdgeLayout(graph);
		      
		    } else if (name.equals("placeEdgeLabels")) {
		      layout = new mxEdgeLabelLayout(graph);
		      
		    } else if (name.equals("organicLayout")) {
		      layout = new mxOrganicLayout(graph);
		      
		    } else if (name.equals("fastOrganicLayout")) {
			      layout = new mxFastOrganicLayout(graph);
			      view.getComponent().setCenterZoom(true);
			    
		    } else if (name.equals("circleLayout")) {
		      layout = new mxCircleLayout(graph);
		      
		      	// center the circle
		        int radius = 220;
		        ((mxCircleLayout) layout).setX0((MainView.DEFAULT_SIZE.width / 2.0) - radius);
		        ((mxCircleLayout) layout).setY0((MainView.DEFAULT_SIZE.height / 2.0) - radius);
		        ((mxCircleLayout) layout).setRadius(radius);
		        ((mxCircleLayout) layout).setMoveCircle(true);
		    }
		  }

		  return layout;
		}
	
	/**
	 * Open the webpage with the given uri
	 * @param uri
	 * @return true if successful and false if not
	 */
	private static boolean openWebpage(URI uri) {
	    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
	        try {
	            desktop.browse(uri);
	            return true;
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	    return false;
	}

	/**
	 * Open the webpage with the given url
	 * @param url
	 * @return true if successful and false if not
	 */
	private static boolean openWebpage(URL url) {
	    try {
	        return openWebpage(url.toURI());
	    } catch (URISyntaxException e) {
	        e.printStackTrace();
	    }
	    return false;
	}
	
	/**
	 * Enables or disables the Addvertex, AddEdge and delete buttons according to the value of parameter
	 * @param b
	 */
	private void setGraphButtonsEnabled(boolean b) {
		view.getButtonAdd().setEnabled(b);
		view.getButtonDel().setEnabled(b);
		view.getButtonEdge().setEnabled(b);
	}
	
	/**
	 * Reset the view 
	 */
	private void resetLayoutView() {
		setGraphButtonsEnabled(true);
		view.getButtonOk().setEnabled(false);
		view.getButtonStart().setEnabled(false);
		view.getMenuLayouts().setEnabled(false);
		view.getTextField().setText("write name of vertex");
		view.getTextPane().setText(view.getDefaultText());
	}
	
	/**
	 * Display an error massage.
	 * @param message
	 * @param image
	 */
	private void displayErrorMessage(String message, BufferedImage image) {
		final ImageIcon icon = new ImageIcon(image);
		JOptionPane.showMessageDialog(view, message, "Error", JOptionPane.ERROR_MESSAGE, icon);
	}
	
	/**
	 * Display an information massage.
	 * @param message
	 * @param image
	 */
	private void displayInfoMessage(String message, BufferedImage image) {
		final ImageIcon icon = new ImageIcon(image);
		JOptionPane.showMessageDialog(view, message, "Info", JOptionPane.INFORMATION_MESSAGE, icon);
	}
	
	/**
	 * Display an question massage.
	 * @param message
	 */
	private int displayQuestionMessage(String message, BufferedImage image) {
		final ImageIcon icon = new ImageIcon(image);
		Object[] options = {"Yes", "No"};
		int n = JOptionPane.showOptionDialog(view, message, "Question",
	    JOptionPane.YES_NO_OPTION,
	    JOptionPane.QUESTION_MESSAGE,
	    icon,
	    options,
	    options[0]);
		return n;
	}
	
	/**
	 * Display an option massage.
	 * @param message
	 */
	private int displayOptionMessage(String message, BufferedImage image,Object[] options) {
		final ImageIcon icon = new ImageIcon(image);
		int n = JOptionPane.showOptionDialog(view, message, "",
	    JOptionPane.YES_NO_OPTION,
	    JOptionPane.QUESTION_MESSAGE,
	    icon,
	    options,
	    options[0]);
		return n;
	}

	/**
	 * Set the style of edge, source and target cells
	 * @param color
	 * @param sourceStrokeWidth
	 * @param graph
	 * @param edgeCell
	 * @param sourceCell
	 * @param targetCell
	 */
	private void setStyle(String edgeColor, String sourceFillColor, String sourceStrokeColor, String sourceStrokeWidth, mxGraph graph, mxCell edgeCell, mxCell sourceCell, mxCell targetCell) {
		//style the edge
		graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, edgeColor, new Object[] {edgeCell});
		graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "5", new Object[] {edgeCell});
		//style the source vertex
		graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, sourceFillColor, new Object[] {sourceCell});
		graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, sourceStrokeWidth, new Object[] {sourceCell});
		graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, sourceStrokeColor, new Object[] {sourceCell});
		//style the target vertex
		graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "2", new Object[] {targetCell});
		graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, redColor, new Object[] {targetCell});
	}
	
	private void resetStyle(mxGraph graph, mxCell sourceCell, mxCell targetCell) {
		if(subtour.get(0).equals(graph.getLabel(targetCell))) {
			if(model.graphIsDirected()) {
				//style the source vertex
				graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "2", new Object[] {sourceCell});
				graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, redColor, new Object[] {sourceCell});
		
			}else {
				if(graph.getLabel(targetCell).equals(subtour.get(subtour.size()-1))) {
					//style the source vertex
					graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "2", new Object[] {sourceCell});
					graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, redColor, new Object[] {sourceCell});
					//style the target vertex
					graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "2", new Object[] {targetCell});
					graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, redColor, new Object[] {targetCell});
				}else if (graph.getLabel(targetCell).equals(subtour.get(subtour.size()-2))) {
					//style the source vertex
					graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "1", new Object[] {sourceCell});
					graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, defaultStrokeColor, new Object[] {sourceCell});
					//style the target vertex
					graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "2", new Object[] {targetCell});
					graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, redColor, new Object[] {targetCell});
				
				}
			}
					
		}else if(subtour.get(0).equals(graph.getLabel(sourceCell))) {
			if(model.graphIsDirected()) {
				//style the target vertex
				graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "1", new Object[] {targetCell});
				graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, defaultStrokeColor, new Object[] {targetCell});
			}else {
				if(graph.getLabel(sourceCell).equals(subtour.get(subtour.size()-1))) {
					//style the source vertex
					graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "2", new Object[] {sourceCell});
					graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, redColor, new Object[] {sourceCell});
					//style the target vertex
					graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "2", new Object[] {targetCell});
					graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, redColor, new Object[] {targetCell});
				}else if (graph.getLabel(sourceCell).equals(subtour.get(subtour.size()-2))) {
					//style the source vertex
					graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "1", new Object[] {targetCell});
					graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, defaultStrokeColor, new Object[] {targetCell});
					//style the target vertex
					graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "2", new Object[] {sourceCell});
					graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, redColor, new Object[] {sourceCell});
				
				}
			}
		}else {
			//the the case of undirected graph
			if(graph.getLabel(targetCell).equals(subtour.get(subtour.size()-1))) {
				//style the source vertex
				graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "2", new Object[] {sourceCell});
				graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, redColor, new Object[] {sourceCell});
				//style the target vertex
				graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "1", new Object[] {targetCell});
				graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, defaultStrokeColor, new Object[] {targetCell});
			
			}else if (graph.getLabel(targetCell).equals(subtour.get(subtour.size()-2))) {
				//style the source vertex
				graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "1", new Object[] {sourceCell});
				graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, defaultStrokeColor, new Object[] {sourceCell});
				//style the target vertex
				graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "2", new Object[] {targetCell});
				graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, redColor, new Object[] {targetCell});
			
			}
		}
	}
	
	private void removeEdgeAndResetStyle(mxGraph graph, mxCell edgeCell, mxCell sourceCell, mxCell targetCell) {
		graph.removeCells(new Object[] {edgeCell});
		if(subtour.get(0).equals(graph.getLabel(targetCell))) {
			//style the source vertex
			graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "2", new Object[] {sourceCell});
			graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, redColor, new Object[] {sourceCell});
			
		}else if(subtour.get(0).equals(graph.getLabel(sourceCell))) {
			//style the target vertex
			graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "1", new Object[] {targetCell});
			graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, defaultStrokeColor, new Object[] {targetCell});
			
		}else {
			//style the source vertex
			graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "2", new Object[] {sourceCell});
			graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, redColor, new Object[] {sourceCell});
			//style the target vertex
			graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "1", new Object[] {targetCell});
			graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, defaultStrokeColor, new Object[] {targetCell});
		}
	}
	
	/**
	 * Reset the style of the selected edge cell
	 * @param graph
	 * @param edgeCell
	 * @param sourceCell
	 * @param targetCell
	 */
	private void resetStyle(mxGraph graph, mxCell edgeCell, mxCell sourceCell, mxCell targetCell) {
		previousEdgeColor = String.valueOf(graph.getCellStyle(edgeCell).get("strokeColor"));
		resetCounter();
		//style the edge
		graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, defaultStrokeColor, new Object[] {edgeCell});
		graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "1", new Object[] {edgeCell});
		
		if (subtour.size()==2) {
			//style the source vertex
			graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, defaultFillColor, new Object[] {sourceCell});
			graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "1", new Object[] {sourceCell});
			graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, defaultStrokeColor, new Object[] {sourceCell});
			//style the target vertex
			graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, "1", new Object[] {targetCell});
			graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, defaultStrokeColor, new Object[] {targetCell});
			subtour.remove(subtour.size()-1);subtour.remove(subtour.size()-1);
			subtourEdges.remove(subtourEdges.size()-1);
			startVertexSelected=false;
			
		}else {
			int countEdges = 0;
			if(model.graphIsDirected()) {
				Object[] s = graph.getEdgesBetween(sourceCell, targetCell, true);
				
				for(int f = 0;f<s.length;f++) {
					mxCell c = (mxCell) s[f];
					if(c.isVisible()) {
						countEdges++;
					}
				}
			}else {
				Object[] s = graph.getEdgesBetween(sourceCell, targetCell);
				
				for(int f = 0;f<s.length;f++) {
					mxCell c = (mxCell) s[f];
					if(c.isVisible()) {
						countEdges++;
					}
				}
			}
			
			if((countEdges==1 || (countEdges>1 && !withoutMatchedEdges))) {
				resetStyle(graph, sourceCell, targetCell);
			}else if ((countEdges>1) && withoutMatchedEdges && !model.isMultiGraph()) {
				removeEdgeAndResetStyle(graph, edgeCell, sourceCell, targetCell);
			}else if ((countEdges>1) && withoutMatchedEdges && model.isMultiGraph()) {
				if(edgeCell.getId().startsWith("NEW")) {
					removeEdgeAndResetStyle(graph, edgeCell, sourceCell, targetCell);
				}else {
					resetStyle(graph, sourceCell, targetCell);
				}
			}
			//remove the last vertex from the subtour
			subtour.remove(subtour.size()-1);
			subtourEdges.remove(subtourEdges.size()-1);
		}
		
	}
	
	/**
	 * Show the actual eulerian cycle
	 */
	private void showEulerianCycle() {
		String tour = "";
		int count = -1;
		for(int i = 0; i<subtour.size();i++) {
			if(subtour.get(i).equals(subtour.get(0))) {
				if(i==0) {
					tour = tour.concat("<font face = \"Arial\" size = \"4\" color=\""+colMap.get(0)+"\">"+subtour.get(0)+" -> </font>");
					tour = tour.concat("<font face = \"Arial\" size = \"4\" color=\""+colMap.get(0)+"\">"+subtour.get(1)+"</font>");
					++i;
					++count;
					continue;
				}else{
					tour = tour.concat("<font face = \"Arial\" size = \"4\" color=\""+colMap.get(count)+"\"> -> "+subtour.get(i)+"</font>");
					++count;
					continue;
				}
				
			}
			tour = tour.concat("<font face = \"Arial\" size = \"4\" color=\""+colMap.get(count)+"\"> -> "+subtour.get(i)+"</font>");
		}
		view.getTextPane().setText(view.getTitleOfTextArea()+"Click on the edges to select them and add them to the eulerian cycle!<br><br>You can click on the <b>\"Cancel last move\"</b> button to cancel your last move or click on the <b>\"Reset\"</b> button to restart building the eulerian cycle from the beginning.<br><br><b>Eulerian cycle:</b><br>["+tour+"]<br><br><b>Actual Cost</b> = "+actualCost+"<br><b>Required Cost</b> = "+totalCost);
	}
	
	/**
	 * Reset the counter to the appropriate value according to the actual color of the edge
	 */
	private void resetCounter() {
		count = colourMap.get(previousEdgeColor);
	}
	
	/**
	 * Fill the selected cell with green color
	 * @param o
	 * @param graph
	 */
	private void fillGreen(Object[] o, mxGraph graph) {
		for (int i = 0;i<o.length;i++) {
			mxCell cell = ((mxCell)o[i]);
			if(cell.isVertex()) {
				if(!graph.getCellStyle(o[i]).get("fillColor").equals(greenColor)) {
					cellsToDelete.add(cell);
					graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, greenColor, new Object[] {o[i]});
				}else {
					graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, defaultFillColor, new Object[] {o[i]});
					cellsToDelete.remove(cell);
				}
			}else if (cell.isEdge()) {
				if(!graph.getCellStyle(o[i]).get("strokeColor").equals(greenColor)) {
					cellsToDelete.add(cell);
					graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, greenColor, new Object[] {o[i]});
				}else {
					graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, defaultStrokeColor, new Object[] {o[i]});
					cellsToDelete.remove(cell);
				}
			}
		}	
	}
	
	/**
	 * Select an edge on the graph
	 * @param o
	 * @param graph
	 */
	private void selectCell(Object[] o, mxGraph graph) {
		
		if(!eulerTourCanStart) {
			fillGreen(o, graph);
		}else if(!withoutMatchedEdges){
			for (int i = 0;i<o.length;i++) {
				mxCell cell = (mxCell)o[i];
				if(cell.isVertex()){
					displayErrorMessage("Only edges can be selected!!!", Smiling_With_Sweat_Emoji);
				}else if(cell.isEdge()){
					mxCell sourceCell = (mxCell) cell.getSource();
					mxCell targetCell = (mxCell) cell.getTarget();
					if (!startVertexSelected) {
						setStyle(blueColor, greenColor, redColor, "2", graph, cell, sourceCell, targetCell);
						//add vertices to the subtour
						subtour.add(graph.getLabel(sourceCell));
						subtour.add(graph.getLabel(targetCell));
						subtourEdges.add(cell);
						actualCost+=Double.parseDouble(graph.getLabel(cell));
						showEulerianCycle();
						startVertexSelected = true;
						
					}else {
						Map<String, Object> style = graph.getCellStyle(cell);
						Object ob = style.get("strokeWidth");
						//if the edge have not been visited yet
						if(!String.valueOf(ob).equals("5")) {
							setColor(graph, cell, sourceCell, targetCell, false) ;
							
						}else {
							if (model.graphIsDirected()) {
								//if the selected edge is the last we added to the subtour
								if(subtour.get(subtour.size()-1).equals(graph.getLabel(targetCell)) && subtour.get(subtour.size()-2).equals(graph.getLabel(sourceCell))) {
									//reset the style to the previous and remove the edge from the subtour
									resetStyle(graph, cell, sourceCell, targetCell);
									actualCost-=Double.parseDouble(graph.getLabel(cell));
									showEulerianCycle();
									
								}else {
									displayErrorMessage("You can only reset the last edge you choosed!!!", Smiling_With_Sweat_Emoji);
								}
							}else {
								if(subtourEdges.get(subtourEdges.size()-1).equals(cell)) {
									//reset the style to the previous and remove the edge from the subtour
									resetStyle(graph, cell, sourceCell, targetCell);
									actualCost-=Double.parseDouble(graph.getLabel(cell));
									showEulerianCycle();
								}else {
									displayErrorMessage("You can only reset the last edge you choosed!!!", Smiling_With_Sweat_Emoji);
								}
							}
						}
					}
				}
			}
		}else {
			for (int i = 0;i<o.length;i++) {
				mxCell cell = (mxCell)o[i];
				if(cell.isVertex()){
					displayErrorMessage("Only edges can be selected!!!", Smiling_With_Sweat_Emoji);
				}else if(cell.isEdge()){
					mxCell sourceCell = (mxCell) cell.getSource();
					mxCell targetCell = (mxCell) cell.getTarget();
					if (!startVertexSelected) {
						setStyle(blueColor, greenColor, redColor, "2", graph, cell, sourceCell, targetCell);
						//add vertices to the subtour
						subtour.add(graph.getLabel(sourceCell));
						subtour.add(graph.getLabel(targetCell));
						subtourEdges.add(cell);
						actualCost+=Double.parseDouble(graph.getLabel(cell));
						showEulerianCycle();
						startVertexSelected = true;
					
					}else {
						Map<String, Object> style = graph.getCellStyle(cell);
						Object ob = style.get("strokeWidth");
						//if the edge have not been visited yet
						if(!String.valueOf(ob).equals("5")) {
							setColor(graph, cell, sourceCell, targetCell, false) ;
							
						}else {
							//if the selected edge is the last we added to the subtour
							if(subtourEdges.get(subtourEdges.size()-1).equals(cell)) {
								if(!model.graphIsDirected()) {
									Object[] s = graph.addAllEdges(new Object[] {targetCell});
									
									if(s.length==1) {
										setColor(graph, cell, sourceCell, targetCell, true) ;	
									}else if(s.length>1) {
										
										int answer = displayQuestionMessage("Do you want to back off your move?\nIf not, the selected edge will be visited once again.", think_Emoji);
										if(answer==0) {
											resetStyle(graph, cell, sourceCell, targetCell);
											actualCost -= Double.parseDouble(graph.getLabel(cell));
											showEulerianCycle();
										}else if (answer==1) {
											setColor(graph, cell, sourceCell, targetCell, true) ;
										}
									}
								}else {
									
									resetStyle(graph, cell, sourceCell, targetCell);
									actualCost -= Double.parseDouble(graph.getLabel(cell));
									showEulerianCycle();

								}

							}else {
								setColor(graph, cell, sourceCell, targetCell, true) ;	
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Set the edge colour for new cycles
	 * @param count
	 * @param graph
	 * @param edgeCell
	 * @param sourceCell
	 * @param targetCell
	 */
	private void setNewCycleColor(int count, mxGraph graph, mxCell edgeCell, mxCell sourceCell, mxCell targetCell) {
		if(count<colMap.size()) {
			setStyle(colMap.get(count), greenColor, redColor, "2", graph, edgeCell, sourceCell, targetCell);
			previousEdgeColor = colMap.get(count);
		}else {
			int index = (int) (count-(Math.round(+Math.random()*colMap.size())));
			setStyle(colMap.get(index), greenColor, redColor, "2", graph, edgeCell, sourceCell, targetCell);
			previousEdgeColor = colMap.get(index);
		}
		actualCost+=Double.parseDouble(graph.getLabel(edgeCell));
		subtour.add(graph.getLabel(targetCell));
		subtourEdges.add(edgeCell);
		showEulerianCycle();
		resetCounter();
	}
	
	/**
	 * set the color of edges
	 * @param graph
	 * @param edgeCell
	 * @param sourceCell
	 * @param targetCell
	 * @param edgeVisited
	 */
	private void setColor(mxGraph graph, mxCell edgeCell, mxCell sourceCell, mxCell targetCell, boolean edgeVisited) {
		//if it comes from the actual source (the last element in the subtour list)
		if (subtour.get(subtour.size()-1).equals(graph.getLabel(sourceCell))) {
			//if its source vertex is not the start vertex 
			if (!graph.getCellStyle(sourceCell).get("fillColor").equals(greenColor)) {
				//if its target vertex is not the start vertex 
				if (!graph.getCellStyle(targetCell).get("fillColor").equals(greenColor)) {
					//if edge is not visited and the user choosed either to add the matched edges or not 
					if((!withoutMatchedEdges && !edgeVisited)||(withoutMatchedEdges && !edgeVisited)) {
						
						if (count < colMap.size()) {
							setStyle(colMap.get(count), defaultFillColor, defaultStrokeColor, "1", graph, edgeCell, sourceCell, targetCell);
							previousEdgeColor = colMap.get(count);
							System.out.println(count);
				
						}else {
							int index = (int) (count-(Math.round(+Math.random()*colMap.size())));
							setStyle(colMap.get(index), defaultFillColor, defaultStrokeColor, "1", graph, edgeCell, sourceCell, targetCell);
							previousEdgeColor = colMap.get(index);
						}
						
						//add target vertex to the subtour
						subtour.add(graph.getLabel(targetCell));
						subtourEdges.add(edgeCell);
						actualCost+=Double.parseDouble(graph.getLabel(edgeCell));
						showEulerianCycle();
						resetCounter();
					//if edge is already visited and the user choosed not to add the matched edges
					}else if(withoutMatchedEdges && edgeVisited) {
						Object[] s = graph.getEdgesBetween(sourceCell, targetCell, true);
						boolean allVisited = true;
						
						for(int f = 0;f<s.length;f++) {
							mxCell c = (mxCell) s[f];
							Map<String, Object> styl = graph.getCellStyle(c);
							Object obj = styl.get("strokeWidth");
							//if of the edge have not been visited yet
							if(!String.valueOf(obj).equals("5") && c.isVisible()) {
								
								allVisited=false;
								break;
							}
						}
						//if all the edges between source and target have already been visited
						if(allVisited) {
							String newEdgeID = model.addEdge(model.getVertexByName(graph.getLabel(sourceCell)), model.getVertexByName(graph.getLabel(targetCell)), graph.getLabel(edgeCell), "new", true);
							Object[] cells = graph.getChildCells(graph.getDefaultParent());
							Object newEdgeCell = null;

							for(int k = 0; k<cells.length; k++) {
								mxCell cell = (mxCell)cells[k];
								if(cell.isEdge()&& cell.getId().equals(newEdgeID)) {
									newEdgeCell = cells[k];
									break;
								}
							}
							if(newEdgeCell!=null) {
								if (count < colMap.size()) {
									setStyle(colMap.get(count), defaultFillColor, defaultStrokeColor, "1", graph, (mxCell) newEdgeCell, sourceCell, targetCell);
									previousEdgeColor = colMap.get(count);
									System.out.println(count);

								}else {
									int index = (int) (count-Math.round(+Math.random()*8));
									setStyle(colMap.get(index), defaultFillColor, defaultStrokeColor, "1", graph, (mxCell) newEdgeCell, sourceCell, targetCell);
									previousEdgeColor = colMap.get(index);
								}
							
							
								this.createLayout("parallelEdges").execute(graph.getDefaultParent());
								//add target vertex to the subtour
								subtour.add(graph.getLabel(targetCell));
								subtourEdges.add((mxCell)newEdgeCell);
								actualCost+=Double.parseDouble(graph.getLabel(edgeCell));
								showEulerianCycle();
								resetCounter();
							
							}
						}else displayErrorMessage("Choose the unvisited edges first", Neutral_Emoji);
				
					}
				}else {
					if((!withoutMatchedEdges && !edgeVisited)||(withoutMatchedEdges && !edgeVisited)) {
					// set its color to the color of the previous edge
					setStyle(previousEdgeColor, defaultFillColor, defaultStrokeColor, "1", graph, edgeCell, sourceCell, targetCell);
					//add target vertex to the subtour
					subtour.add(graph.getLabel(targetCell));
					subtourEdges.add(edgeCell);
					actualCost+=Double.parseDouble(graph.getLabel(edgeCell));
					showEulerianCycle();
					resetCounter();
					}else if(withoutMatchedEdges && edgeVisited) {
						Object[] s = graph.getEdgesBetween(sourceCell, targetCell, true);
						boolean allVisited = true;
						
						for(int f = 0;f<s.length;f++) {
							mxCell c = (mxCell) s[f];
							Map<String, Object> styl = graph.getCellStyle(c);
							Object obj = styl.get("strokeWidth");
							//if the edge have not been visited yet
							if(!String.valueOf(obj).equals("5") && c.isVisible()) {
								allVisited=false;
								break;
							}
						}
						if(allVisited) {
							String newEdgeID =  model.addEdge(model.getVertexByName(graph.getLabel(sourceCell)), model.getVertexByName(graph.getLabel(targetCell)), graph.getLabel(edgeCell), "new", true);
							Object[] cells = graph.getChildCells(graph.getDefaultParent());
							Object newEdgeCell = null;
						
							for(int k = 0; k<cells.length; k++) {
								mxCell cell = (mxCell)cells[k];
								if(cell.isEdge()&& cell.getId().equals(newEdgeID)) {
									newEdgeCell = cells[k];
									break;
								}
							}
							if(newEdgeCell!=null) {
								// set the its color to the color of the previous edge
								setStyle(previousEdgeColor, defaultFillColor, defaultStrokeColor, "1", graph, (mxCell)newEdgeCell, sourceCell, targetCell);
				
								//add target vertex to the subtour
								subtour.add(graph.getLabel(targetCell));
								subtourEdges.add((mxCell)newEdgeCell);
								actualCost+=Double.parseDouble(graph.getLabel(edgeCell));
								showEulerianCycle();
								resetCounter();
								this.createLayout("parallelEdges").execute(graph.getDefaultParent());
							}
						}else displayErrorMessage("Choose the unvisited edges first", Neutral_Emoji);
		
					}
					
				}

			}else {
				if(model.graphIsDirected()) {
					int max = Integer.MIN_VALUE;
					//reset the counter value to the one for the previous cycle
					for(Object edge:graph.getIncomingEdges(sourceCell)) {
						mxCell c = (mxCell) edge;
						Map<String, Object> styl = graph.getCellStyle(c);
						Object obj = styl.get("strokeColor");
						Object o = styl.get("strokeWidth");
						//if the edge have not been visited yet
						if(String.valueOf(o).equals("5") && c.isVisible()) {
							if(colourMap.get(String.valueOf(obj))>max) {
								max = colourMap.get(String.valueOf(obj));
							}
						}
					}
					count=max;
					
				}else {
					if(
						subtour.get(subtour.size()-2).equals(graph.getLabel(sourceCell))^subtour.get(subtour.size()-1).equals(graph.getLabel(sourceCell))) {
						int max = Integer.MIN_VALUE;
						for(Object edge:graph.getEdges(sourceCell)) {

							mxCell c = (mxCell) edge;
							Map<String, Object> styl = graph.getCellStyle(c);
							Object o = styl.get("strokeWidth");
							Object obj = styl.get("strokeColor");

							//if the edge have not been visited yet
							if(String.valueOf(o).equals("5") && c.isVisible()) {
								if(colourMap.get(String.valueOf(obj))>max) {
									max = colourMap.get(String.valueOf(obj));
								}
							}
						}
						count=max;

					}
				}
				//increment the counter to set the edge color the another one for the new cycle
				++count;
				if((!withoutMatchedEdges && !edgeVisited)||(withoutMatchedEdges && !edgeVisited)) {
					setNewCycleColor(count, graph, edgeCell, sourceCell, targetCell);
					}else if(withoutMatchedEdges && edgeVisited) {
						Object[] s = graph.getEdgesBetween(sourceCell, targetCell, true);
						boolean allVisited = true;
						
						for(int f = 0;f<s.length;f++) {
							mxCell c = (mxCell) s[f];
							Map<String, Object> styl = graph.getCellStyle(c);
							Object obj = styl.get("strokeWidth");
							//if the edge have not been visited yet
							if(!String.valueOf(obj).equals("5") && c.isVisible()) {
								allVisited=false;
								break;
							}
						}
						if(allVisited) {
							String newEdgeID =  model.addEdge(model.getVertexByName(graph.getLabel(sourceCell)), model.getVertexByName(graph.getLabel(targetCell)), graph.getLabel(edgeCell), "new", true);
							Object[] cells = graph.getChildCells(graph.getDefaultParent());
							Object newEdgeCell = null;
							for(int k = 0; k<cells.length; k++) {
								mxCell cell = (mxCell)cells[k];
								if(cell.isEdge()&& cell.getId().equals(newEdgeID)) {
									newEdgeCell = cells[k];
									break;
								}
							}
							if(newEdgeCell!=null) {
								setNewCycleColor(count, graph, (mxCell)newEdgeCell, sourceCell, targetCell);						
								this.createLayout("parallelEdges").execute(graph.getDefaultParent());

							}
						}else displayErrorMessage("Choose the unvisited edges first", Neutral_Emoji);
						
					}
			}
			
			if(totalCost != actualCost) {
				Object[] s = graph.getOutgoingEdges(targetCell);
				boolean allVisited = true;
				if(totalCost > actualCost) {
					if(!withoutMatchedEdges) {
						if(model.graphIsDirected()) {
							for(int f = 0;f<s.length;f++) {
								
								mxCell c = (mxCell) s[f];
								Map<String, Object> styl = graph.getCellStyle(c);
								Object obj = styl.get("strokeWidth");
								//if the edge have not been visited yet
								if(!String.valueOf(obj).equals("5")) {
									allVisited=false;
									break;
								}
							}
							if (allVisited) {
								
								displayErrorMessage("There is no more move possible from "+graph.getLabel(targetCell)+" !!!", Smiling_With_Sweat_Emoji);
								displayErrorMessage("There is still hope!!! \nTry to cancel some of your moves and try again", wink_Emoji);
							}
						}
					}
				}else {
					displayErrorMessage("The actual cost has exceeded !!!", crying_Emoji);
					displayErrorMessage("There is still hope!!! \nTry to cancel some of your moves and try again", wink_Emoji);
				}
				
			}else {
				Object[] s = graph.getChildEdges(graph.getDefaultParent());
				boolean allVisited = true;
				
				for(int f = 0;f<s.length;f++) {
					
					mxCell c = (mxCell) s[f];
					Map<String, Object> styl = graph.getCellStyle(c);
					Object obj = styl.get("strokeWidth");
					//if the edge have not been visited yet
					if(!String.valueOf(obj).equals("5")) {
						allVisited=false;
						break;
					}
				}
				if (allVisited) {
					if(subtour.get(0).equals(graph.getLabel(targetCell))){
						displayInfoMessage("Congratulation!!!\n\nI am so glad you found the smallest distance.\nI am going to deliver these mails right now.\nJust create a new graph if there is a new commission.\n\nSee you soon!", postboten);
					}
				}
			}
			
		}else {
			//if the edge is undirected, test the selected edge comes out from last added "target cell"(actually the source cell since the graph is undirected) in the subtour
			if(!model.graphIsDirected() && subtour.get(subtour.size()-1).equals(graph.getLabel(targetCell))){
				//swap source and target cells
				setColor(graph, edgeCell, targetCell, sourceCell, edgeVisited);
			}else {
				displayErrorMessage("Select an edge outgoing from the actual source (the one with the red rand),\nOr click on the last edge you've added to remove it.", Smiling_With_Sweat_Emoji);
			}
		}
	}
	
	/**Load the image with the specified name from the project
	 * @param name
	 * @return a buffered image or null
	 */
	private BufferedImage load(String name) {
		URL url = getClass().getResource(name);
		BufferedImage image = null;
		try {
			image = ImageIO.read(url);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return image;
	}

	/**
	 * @param cpp the cpp to set
	 */
	public void setCpp(Algorithms cpp) {
		this.cpp = cpp;
	}
	
}