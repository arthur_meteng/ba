/**
 * 
 */
package main;

import java.awt.Color;
import java.awt.Font;

import javax.swing.UIManager;

import View.MainView;
import controller.Controller;
import model.Model;

/**
 * @author PaulArthur
 *
 */
public class MainCPG {
	private final static Color lightGray = new Color(220,220,220);

	/**
	 * 
	 */
	public MainCPG() {
		// TODO Auto-generated constructor stub
	}
	
	public static void main (String[] args) {
		UIManager.put("OptionPane.messageFont", new Font("Arial", Font.BOLD, 12));
		UIManager.put("OptionPane.buttonFont", new Font("Arial", Font.BOLD, 12));
		UIManager.put("OptionPane.background", lightGray);
		UIManager.put("OptionPane.foreground", lightGray);
		Model model = new Model();
		MainView view = new MainView(model);
		new Controller(view, model);

		view.setVisible(true);

	}

}
